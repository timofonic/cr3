#include <sys/time.h>

#include <iostream>

#include <QFile>
#if !defined(IOS)
#include <QProcess>
#endif
#include <QString>
#include <QFileDialog>
#include <QKeyEvent>
#include <QApplication>
#include <QDesktopWidget>
#include <QTextStream>
#include <QComboBox>
#include <QAbstractSpinBox>
#include <QAbstractItemView>
#include <QLabel>
#include <QCheckBox>
#include <QGroupBox>
#include <QTextEdit>
#include <QPlainTextEdit>
#include <QToolButton>
#include <QStyledItemDelegate>
#include <QDebug>
#include <QTableView>
#include <QTextBrowser>
#if defined(ANDROID) || defined(SIMPLE) || defined(IOS)
#include <QScroller>
#endif

#include "utils.h"
#include "version.h"
#if !defined(DESKTOP)
 #include "FileOpenDialog.h"
 #include "FileSaveDialog.h"
 #if defined(Q_WS_QWS)
  #include "gesturescontroller.h"
 #endif
#endif
#include "Config.h"
#include "Dialog.h"
#include "Platform.h"
#include "Log.h"
#if defined(Q_WS_QWS)
 #include <QTransformedScreen>
 #include <QWSDisplay>
#endif

#include "LangUtils.h"

#include "QtUtils.h"

QString v1()
{
    ::srand ( time(NULL) );
    QByteArray buf(8,'\0');
    for ( int n=0; n<buf.size(); ++n )
    {
        buf[n] = '0' + (::rand() % 10);
    }
    return QString::fromLatin1(buf.constData());
}

void centerWidget( QWidget* w, const QRect& r )
{
    w->move( ( r.width() - w->width() ) / 2, ( r.height() - w->height() ) / 2 );
}

void centerWidget( QWidget* w )
{
    QRect r(QApplication::desktop()->screenGeometry());
    centerWidget(w, r);
}

void messageBox( QWidget* parent,
                 QMessageBox::Icon icon,
                 const QString& title,
                 const QString& text )
{
    QMessageBox(icon,title,text,QMessageBox::Ok,parent).exec();
    Platform::get()->scheduleEinkFullUpdate();
}

void messageBox( QWidget* parent,
                 QMessageBox::Icon icon,
                 const char* title,
                 const char* text )
{
    messageBox( parent, icon, QString::fromUtf8(title), QString::fromUtf8(text) );
}

QMessageBox::StandardButton questionBox(QWidget *parent, const QString &title, const QString &text, QMessageBox::StandardButtons buttons, QMessageBox::StandardButton defaultButton)
{
    QMessageBox::StandardButton r = QMessageBox::question(parent, title, text, buttons, defaultButton);
    Platform::get()->scheduleEinkFullUpdate();
    return r;
}

QString getOpenFileName(QWidget* parent, const QString& caption, const QString& path, const QString& filter)
{
    QString fn;
#ifdef DESKTOP
    QFileDialog dlg(parent, caption, path);
    dlg.setNameFilter(filter);
    dlg.setFileMode(QFileDialog::ExistingFile);
    dlg.setAcceptMode(QFileDialog::AcceptOpen);
    if ( dlg.exec() )
    {
        QStringList files(dlg.selectedFiles());
        if ( !files.empty() )
            fn = files.at(0);
    }
#else
    Q_UNUSED(caption);
    FileOpenDialog dlg(parent, path, filter);
    if ( dlg.exec() )
    {
        fn = dlg.getFileName();
    }
#endif
    return fn;
}

QString getSaveFileName(QWidget* parent, const QString& caption, const QString& path, const QString& filter, const QString& defSuffix)
{
    QString fn;
#ifdef DESKTOP
    QFileDialog dlg(parent, caption, path);
    dlg.setDefaultSuffix(defSuffix);
    dlg.setNameFilter(filter);
    dlg.setFileMode(QFileDialog::AnyFile);
    dlg.setAcceptMode(QFileDialog::AcceptSave);
    if ( dlg.exec() )
    {
        QStringList files(dlg.selectedFiles());
        if ( !files.empty() )
            fn = files.at(0);
    }
#else
    Q_UNUSED(caption);
    FileSaveDialog dlg(parent, path, filter);
    dlg.setDefaultSuffix(defSuffix);
    if ( dlg.exec() )
    {
        fn = dlg.getFileName();
    }
#endif
    return fn;
}

QString get_time_as_string(int ms)
{
    char buf[128];
    char sig[2] = {'\0'};
    if ( ms < 0 )
    {
        ms = abs(ms);
        sig[0] = '-';
    }
    int r = ms - ( ms / 1000 ) * 1000;
    int s = ms / 1000 + (r > 500? 1:0);
    int secs  = s % 60;
    int mins  = ( s / 60   ) % 60;
    int hours = ( s / 3600 ) % 24;
    int days  = s / 3600 / 24;
    if ( days > 0 )
        ::snprintf(buf,SIZE(buf),"%s%dd %02d:%02d:%02d",sig,days,hours,mins,secs);
    else
        ::snprintf(buf,SIZE(buf),"%s%02d:%02d:%02d",sig,hours,mins,secs);
    return buf;
}

qint64 msecsTo(const QDateTime& t1, const QDateTime& t2)
{
    qint64 days = t1.daysTo(t2);
    qint64 msecs = t1.time().msecsTo(t2.time());
    return days*(1000*3600*24) + msecs;
}

void touch_file(const QString &fileName)
{
    QFile file( fileName );
    file.open(QIODevice::WriteOnly );
    file.close();
}

QString str_from_file( const QString& fileName )
{
    QString result;
    QFile file( fileName );
    if( file.open(QIODevice::ReadOnly | QIODevice::Text) )
    {
        QTextStream in(&file);
        result = in.readLine().trimmed();
    }
    return result;
}

int int_from_file( const QString& fileName )
{
    return str_from_file(fileName).toInt();
}

#if !defined(IOS)
QString str_from_proc( const QString& cmd )
{
    QString result;
    QProcess proc;
    proc.start(cmd);
    if ( proc.waitForReadyRead(1000) )
    {
        result = proc.readAll();
    }
    proc.waitForFinished();
    return result;
}
#endif

void str_to_file( const QString& fileName, const QString& str )
{
    QFile file(fileName);
    if (file.open(QIODevice::WriteOnly))
    {
        QTextStream out(&file);
        out << str << endl;
    }
}

void initTopLevelWidget( QWidget* w )
{
#if !defined(DESKTOP)
    QList<QWidget*> widgets = w->findChildren<QWidget *>();
    for ( QList<QWidget*>::iterator
          i  = widgets.begin();
          i != widgets.end();
          ++i )
    {
        QAbstractItemView* pIv = qobject_cast<QAbstractItemView*>(*i);
        if ( pIv )
        {
#if defined(SIMPLE) || defined(ANDROID) || defined(IOS)
            pIv->setHorizontalScrollMode(QAbstractItemView::ScrollPerPixel);
            pIv->setVerticalScrollMode(QAbstractItemView::ScrollPerPixel);
#else
            (*i)->installEventFilter(w);
#endif
            ::activate_scroller( qobject_cast<QAbstractScrollArea*>(*i) );
            continue;
        }
        if (    qobject_cast<QTextEdit*>(*i)
             || qobject_cast<QPlainTextEdit*>(*i) )
        {
#if !defined(ANDROID) && !defined(SIMPLE) && !defined(IOS)
            (*i)->installEventFilter(w);
#endif
            ::activate_scroller(qobject_cast<QAbstractScrollArea*>(*i));
            continue;
        }
        if (    qobject_cast<QLineEdit*>(*i)
             || qobject_cast<QAbstractItemView*>(*i)
             || qobject_cast<QSlider*>(*i) )
        {
#if !defined(ANDROID) && !defined(SIMPLE) && !defined(IOS)
            (*i)->installEventFilter(w);
#endif
            continue;
        }     
        QComboBox* pCb = qobject_cast<QComboBox*>(*i);
        if ( pCb )
        {
#if defined(Q_WS_QWS)
            pCb->setItemDelegate( new QStyledItemDelegate() );
#endif
            pCb->view()->setMouseTracking(false);
#if defined(SIMPLE) || defined(ANDROID) || defined(IOS)
            pCb->view()->setHorizontalScrollMode(QAbstractItemView::ScrollPerPixel);
            pCb->view()->setVerticalScrollMode(QAbstractItemView::ScrollPerPixel);
#endif
            ::activate_scroller( pCb->view() );
            continue;
        }
        QAbstractSpinBox* pAsb = qobject_cast<QAbstractSpinBox*>(*i);
        if ( pAsb )
        {
#if defined(Q_WS_QWS)
            pAsb->setButtonSymbols( QAbstractSpinBox::NoButtons );
            pAsb->installEventFilter(w);
#endif
            continue;
        }
    }
#else
    Q_UNUSED(w)
#endif
}

void writeFifoCommand(const QString& fifo, const char *cmd)
{
    QFile file(fifo);
    if ( file.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Unbuffered) )
    {
        file.write(cmd);
        file.close();
        g_pLog->write(1, Log::MT_I, "writeFifoCommand: %s <== %s", QFile::encodeName(fifo).constData(), cmd );
    }
}

QSize getSizePx(const QSize& centiInches, int screen)
{
    int dpiX = qApp->desktop()->logicalDpiX();
    int dpiY = qApp->desktop()->logicalDpiY();
    QSize size;
    size.setWidth( centiInches.width() * dpiX / 100 );
    size.setHeight( centiInches.height() * dpiY / 100 );
    QDesktopWidget* w = qApp->desktop();
    int min = qMin( w->width(), w->height() ) / screen;
    size.setWidth( qMin( min, size.width() ) );
    size.setHeight( qMin( min, size.height() ) );
    return size;
}

QString get_book_cover_path()
{
    return QFile::decodeName(qgetenv("ROOT")) + QDir::separator() + "cr3" + QDir::separator() + "data" + QDir::separator() + "cache" + QDir::separator() + "cover";
}

bool is_eink()
{
    return qApp->styleSheet().endsWith("eink.qss");
}

int in_to_px( qreal inches )
{
    return static_cast<int>(Platform::get()->getDPI() * inches);
}

int mm_to_px( qreal mm )
{
    return static_cast<int>(Platform::get()->getDPI() * mm / 25.4);
}

#if !defined(DESKTOP)
void activate_scroller( QAbstractScrollArea* pArea )
{
    if ( pArea )
    {
#if defined(Q_WS_QWS)
        new GesturesController(pArea);
#else
        QScroller* scroller = QScroller::scroller(pArea);
        QScrollerProperties prop = scroller->scrollerProperties();
        prop.setScrollMetric(QScrollerProperties::MaximumClickThroughVelocity, 0);
        prop.setScrollMetric(QScrollerProperties::MousePressEventDelay, 0.5);
        scroller->setScrollerProperties(prop);
        scroller->grabGesture(pArea, QScroller::LeftMouseButtonGesture);
#endif
    }
}
#endif


void resize_font(QWidget *w, int nom, int denom)
{
    QFont f(w->font());
    f.setPointSize(f.pointSize()*nom/denom);
    w->setFont(f);
}

QString to_rgba(const QColor &color)
{
    return QString("rgba(")
        +  QString::number(color.red()) + ","
        +  QString::number(color.green()) + ","
        +  QString::number(color.blue()) + ","
        +  QString::number(color.alpha()) + ")";
}

void readTextBrowser(QTextBrowser* textBrowser, const QString& fn)
{
    QString lang(::getLangName());
    QString fileName( Platform::get()->getRootPath() + QDir::separator() + "html" + QDir::separator() + lang + QDir::separator() + fn );
    QFileInfo fileInfo(fileName);
    if ( !fileInfo.isFile() )
    {
        fileName = Platform::get()->getRootPath() + QDir::separator() + "html" + QDir::separator() + "English" + QDir::separator() + fn;
    }
    QFile file(fileName);
    if ( file.open(QFile::ReadOnly | QFile::Text) )
    {
        QTextStream s(&file);
        s.setCodec("UTF-8");
        textBrowser->setHtml(s.readAll());
        textBrowser->setOpenExternalLinks(true);
    }
}
