#ifndef FILEOPENDIALOG_H_
#define FILEOPENDIALOG_H_

#include <QFileSystemModel>
#include <QTreeView>

#include "Dialog.h"

#include "ui_FileOpenDialog.h"

class QKeyEvent;

class FileOpenDialog: public Dialog, private Ui::FileOpenDialog
{
    Q_OBJECT

    QFileSystemModel fsModel;
    QString fileName;
    QString ext;

public:
    FileOpenDialog(QWidget* parent, const QString& path, const QString& exts, bool hidden=false);

    const QString& getFileName() const
    { return fileName; }

private:
    virtual void keyPressEvent( QKeyEvent* e );

private slots:
    virtual void accept();
    void onBtnUp();
    void select( const QModelIndex& index );
    void expand( const QModelIndex& index );
};

#endif /* FILEOPENDIALOG_H_ */
