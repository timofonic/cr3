#include <QCoreApplication>
#include <QSettings>
#include <QDir>
#include <QFile>

#include "utils.h"
#include "Platform.h"

#define CPP__CONFIG

#include "Config.h"

static inline int limit(int val, int min, int max, int def)
{
    if ( max >= min && ( val < min || val > max ) ) val = def;
    return val;
}

Config::Config( const QString& name )
{
    ini = new QSettings(Platform::get()->getUserSettingsPath() + QDir::separator() + name + ".ini", QSettings::IniFormat);
    ini->setIniCodec("UTF-8");
}

Config::~Config()
{
    delete ini;
}

void Config::sync()
{
    ini->sync();
}

int Config::readInt( const char* param, int def, int min, int max ) const
{
    QVariant res(ini->value(param, def));
    bool isOk = false;
    int val = res.toInt(&isOk);
    return isOk? limit(val,min,max,def):def;
}

int Config::readInt( const QString& param, int def, int min, int max ) const
{
    QVariant res(ini->value(param, def));
    bool isOk = false;
    int val = res.toInt(&isOk);
    return isOk? limit(val,min,max,def):def;
}

bool Config::readBool(const QString &param, bool def) const
{
    int val = readInt( param, def? 1:0 );
    return val != 0;
}

std::string Config::readString( const char* param, const char* def ) const
{
    QString res(ini->value(param, QString::fromUtf8(def)).toString());
    return res.toUtf8().constData();
}

QString Config::readQString( const char* param, const QString& def ) const
{
    return ini->value(param, def).toString();
}

QString Config::readQString( const QString& param, const QString& def ) const
{
    return ini->value(param, def).toString();
}

void Config::writeInt( const char* param, int val )
{
    ini->setValue(param, val);
}

void Config::writeInt( const QString& param, int val )
{
    ini->setValue(param, val);
}

void Config::writeBool( const QString& param, bool val )
{
    writeInt( param, val? 1:0 );
}

void Config::writeString( const char* param, const char* value )
{
    ini->setValue(param, QString::fromUtf8(value));
}

void Config::writeQString( const char* param, const QString& value )
{
    ini->setValue(param, value);
}

void Config::writeQString( const QString& param, const QString& value )
{
    ini->setValue(param, value);
}
