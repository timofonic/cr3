#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
#include <ctype.h>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <fstream>

#include "utils.h"

bool is_absolute_path( const char* path )
{
	return path && '/' == path[0];
}

std::string get_ext(const std::string& str)
{
    size_t period = str.find_last_of(".");
    return period == std::string::npos? "":str.substr(period + 1);
}

std::string get_base(const std::string& str)
{
    size_t period = str.find_last_of(".");
    return period == std::string::npos? str:str.substr(0,period);
}

std::string get_file(const std::string& str)
{
    return str.substr(str.find_last_of("/\\") + 1);
}

std::string get_path(const std::string& str)
{
    return str.substr(0, str.find_last_of("/\\"));
}

bool is_file(const char* path)
{
    struct stat fst;
    return (-1 != ::stat(path, &fst) && S_ISREG(fst.st_mode));
}

bool is_dir(const char* path)
{
    struct stat fst;
    return (-1 != ::stat(path, &fst) && S_ISDIR(fst.st_mode));
}

static const char* first_path_delimiter(const char* path)
{
    return strchr(path, PSEP);
}

static const char* skip_absolute_path_prefix(const char* path)
{
    const char* ptr = path;
    if (*ptr == '/') ptr++;
    return ptr;
}

bool mk_dir(const char* /*dir*/)
{
    // TODO: restore
	//return 0 == ::mk_dir(dir, 0755);
	return true;
}

bool mkpath(const char* path, bool skipLast /* = false */)
{
    std::string str;
    const char* ptr = NULL;

    ptr = skip_absolute_path_prefix(path);
    if (0 == ptr)
        return false;

    // Checking for the drive or the network path existance.
    //   This is mostly Windows-specific.
    if ('\0' == *ptr && !is_dir(path))
        return false;

    ptr = first_path_delimiter(ptr);

    for (; 0 != ptr; ptr = first_path_delimiter(ptr + 1))
    {
        str.assign(path, ptr - path);

        // The order is important here. We first try to create a
        //   directory and if it fails we are checking if such a
        //   directory already exists. This helps to avoid problems
        //   when creating the same path concurrently.
        if (!mk_dir(str.c_str()) && !is_dir(str.c_str()))
            return false;

        if ('\0' == *(ptr + 1))
            return true; // path ends with '/'
    }

    if (!skipLast && !mk_dir(path) && !is_dir(path))
        return false;

    return true;
}

void create_empty_file( const char* file )
{
    FILE* fp = ::fopen(file, "w");
    ::fclose(fp);
}

void remove_file( const std::string& file )
{
    ::unlink(file.c_str());
}

int str2int(const char* str, int def)
{
    char* errp;
    int k = strtol(str, &errp, 10);
    if ( *errp != '\0' )
    {
        k = def;
    }
    return k;
}

std::string int2str( int val )
{
    char buff[32];
    ::snprintf(buff, 32, "%d", val);
    return std::string(buff);
}

void splitInVector (const std::string& text,
                    const std::string& separators,
                    std::vector<std::string>& words)
{
    std::string::size_type n = text.length();
    std::string::size_type start = text.find_first_not_of(separators);
    while ( start < n )
    {
        std::string::size_type stop = text.find_first_of(separators, start);
        if ( stop > n ) stop = n;
        words.push_back(text.substr(start, stop - start));
        start = text.find_first_not_of(separators, stop+1);
    }
}

std::string vacuum( const std::string& val )
{
    static const char* spaces = " \n\r\t";
    size_t pos = std::string::npos;
    std::string result(val);

    result.erase(0,result.find_first_not_of(spaces));

    pos = result.find_last_not_of(spaces);
    if ( pos != std::string::npos )
    {
         if ( pos + 1 < val.size() )
         {
             result.erase(pos+1);
         }
    }
    else
        result.erase(0);

    return result;
}

std::string to_upper( const std::string& val )
{
	std::string result(val);
	for (std::string::iterator p = result.begin(); p != result.end(); ++p)
	    *p = ::toupper(*p);
	return result;
}

void str_to_file( const std::string& fileName, const std::string& str )
{
	std::ofstream f(fileName.c_str());
	if ( f.is_open() ) f << str;
}

std::string str_from_file( const std::string& fileName )
{
	std::string result;
    std::ifstream f(fileName.c_str());
    if ( f.is_open() ) getline(f, result);
    return result;
}

#if defined(DESKTOP) && defined(LINUX)
// workaround:
// don't use the original memcpy() on linux to avoid dep
// from the latest libc
void* memcpy(void *dest, const void *src, size_t n)
{
    return memmove(dest,src,n);
}
#endif
