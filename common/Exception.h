#ifndef H__EXCEPTION
#define H__EXCEPTION

#include <cstdarg>
#include <string>
#include <exception>

class Exception : public std::exception
{
    bool isCritical_;

public:
    Exception(bool isCritical, const std::string& message);
    Exception(bool isCritical, const char* pFormat, ...) __attribute__((format(printf, 3, 4)));
    virtual ~Exception() throw();
    virtual const char* what(void) const throw();
    bool isCritical() const;

protected:
    void formatMessage(const char* pFormat, ...);
    void formatMessage(const char* pFormat, va_list ap);

private:
    std::string message_;
};

#endif
