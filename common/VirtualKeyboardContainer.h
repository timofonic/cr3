#ifndef VIRTUALKEYBOARDCONTAINER_H
#define VIRTUALKEYBOARDCONTAINER_H

#include "VirtualKeyboard.h"

class VirtualKeyboardContainer
{
    int extraSize;
    QWidget* parent;
    VirtualKeyboard vk;

public:
    VirtualKeyboardContainer( QWidget* p )
        : extraSize(0)
        , parent(p)
        , vk(p)
    {
        vk.hide();
    }
    virtual ~VirtualKeyboardContainer() {}
    virtual void showVirtualKeyboard( QWidget* w );
    virtual VirtualKeyboard* getVirtualKeyboard()
    { return &vk; }
};

#endif // VIRTUALKEYBOARDCONTAINER_H
