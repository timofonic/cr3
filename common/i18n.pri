TRANSLATIONS +=       \
    i18n/Russian.ts   \
    i18n/Ukrainian.ts \
    i18n/German.ts    \
    i18n/Spanish.ts   \
    i18n/French.ts    \
    i18n/Italian.ts   \
    i18n/Hungarian.ts \
    i18n/Polish.ts    \
    i18n/Czech.ts     \
    i18n/Norwegian.ts \
    i18n/Brazilian.ts \
    i18n/Dutch.ts     \
    i18n/Catalan.ts   \
    i18n/Bulgarian.ts \
    i18n/Turkish.ts   \
    i18n/Urdu.ts

lrelease.input = TRANSLATIONS
lrelease.output = $$DESTDIR/i18n/${QMAKE_FILE_BASE}.qm
lrelease.commands = $$[QT_INSTALL_BINS]/lrelease -silent -compress ${QMAKE_FILE_IN} -qm $$lrelease.output
lrelease.CONFIG += no_link target_predeps
QMAKE_EXTRA_COMPILERS += lrelease
DEPENDPATH += i18n

