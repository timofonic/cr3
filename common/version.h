#ifndef H__VERSION
#define H__VERSION

#include "revision.h"

#define VERSION "2015.10"

// All generated serial numbers become invalid after the sequence change
// It should be done after major release
#define SEQ "1"

#endif

