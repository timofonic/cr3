#ifndef ENTERTEXTLINEDIALOG_H_
#define ENTERTEXTLINEDIALOG_H_

#include "Dialog.h"

#include "ui_EnterTextLineDialog.h"

class EnterTextLineDialog : public Dialog, private Ui::EnterTextLineDialog
{
    Q_OBJECT

public:
    EnterTextLineDialog(QWidget* parent, const QString& title, const QString& prompt, const QString& value = "");

    QString getValue() const
    { return leValue->text(); }

private slots:
#if defined(QT_KEYPAD_NAVIGATION)
    void onSelectText();
#endif
};

#endif /* ENTERTEXTLINEDIALOG_H_ */
