#ifndef H__KINDLE_PLATFORM
#define H__KINDLE_PLATFORM

#include "QWSPlatform.h"

class KindlePlatform : public QWSPlatform
{
public:
    enum Device
    {
        KT,
        KT2,
        KPW,
        KPW2,
        KV,
        OTHER
    };

public:
    KindlePlatform();
    virtual QString getSerialNumber() const;
    virtual int getBatteryLevel() const;
    virtual bool isBatteryCharging() const;
    virtual bool frontlightIsPresent() const;
    virtual int frontlightGetMinLevel() const;
    virtual int frontlightGetMaxLevel() const;
    virtual void frontlightSetLevel( int val );
    virtual void frontlightSetOn( bool val );

private:
    void frontlightStore( int value );

private:
    Device dev;
};

#endif // H__KINDLE_PLATFORM
