#include <arpa/inet.h>
#include <sys/socket.h>
#include <netdb.h>
#include <ifaddrs.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <linux/if_link.h>

#include <algorithm>

#include <QDebug>
#include <QDir>
#include <QScreen>
#include <QWSDisplay>
#include <QTransformedScreen>
#include <QCoreApplication>
#include <QProcess>

#include "QtUtils.h"

#include "QWSPlatform.h"

QWSPlatform::QWSPlatform()
{
    nwTimer.setSingleShot(true);
    nwTimer.setInterval(10*60*1000); // 10 min
    connect(&nwTimer, SIGNAL(timeout()), this, SLOT(networkTimer()));

}

QString QWSPlatform::getUserSettingsPath() const
{
    return getRootPath();
}

QString QWSPlatform::getRootPath() const
{
    return QCoreApplication::applicationDirPath();
}

QString QWSPlatform::getDocumentsPath() const
{
    return getRootPath();
}

QString QWSPlatform::getCommonTranslationsPath() const
{
    return qgetenv("VLASOVSOFT_I18N");
}

int QWSPlatform::getOrientation() const
{
    return QScreen::instance()->transformOrientation();
}

void QWSPlatform::setOrientation(int angle)
{
    QWSDisplay::setTransformation(static_cast<QTransformedScreen::Transformation>(angle), 0);
}

bool QWSPlatform::canRotateScreen() const
{
    QScreen* pScreen = QScreen::instance();
    return pScreen->depth() > 4 && QScreen::TransformedClass == pScreen->classId();
}

int QWSPlatform::getBatteryLevel() const
{
    return int_from_file(QString(qgetenv("ROOT")) + QDir::separator() + "batt_level.txt");
}

bool QWSPlatform::isBatteryCharging() const
{
    return str_from_file(QString(qgetenv("ROOT")) + QDir::separator() + "batt_status.txt") == "Charging";
}

void QWSPlatform::scheduleEinkFullUpdate() const
{
    QScreen* pScreen = QScreen::instance();
    pScreen->setDirty(QRect(0,0,pScreen->width(),pScreen->height()));
}

int QWSPlatform::getDefaultFontSize() const
{
    QScreen* pScreen = QScreen::instance();
    int fs = static_cast<int>(0.5 + std::min(pScreen->physicalWidth(),pScreen->physicalHeight()) * 72.0 / 25.4 / 18.0);
    return std::min(fs,15); // 15 points ~ 5.25 mm
}
    
QString QWSPlatform::getSystemLang() const
{
    return qgetenv("VLASOVSOFT_LNG");
}

qreal QWSPlatform::getDPI() const
{
    QScreen* pScreen = QScreen::instance();
    qreal dpiX = 25.4 * pScreen->width()  / pScreen->physicalWidth();
    qreal dpiY = 25.4 * pScreen->height() / pScreen->physicalHeight();
    return (dpiX + dpiY)/2.0;
}

bool QWSPlatform::isNetworkActive() const
{
    ifaddrs *ifaddr, *ifa;

    if (getifaddrs(&ifaddr) == -1) {
        return false;
    }

    int n = 0;
    for (ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next, n++)
    {
        if (ifa->ifa_addr == NULL)
            continue;

        if ( 0 == strcmp( ifa->ifa_name, "lo" ) )
            continue;

        int family = ifa->ifa_addr->sa_family;
        if (family == AF_INET || family == AF_INET6)
        {
            return true;
        }
    }

    return false;
}

void QWSPlatform::switchNetworkOn()
{
    runScript("wifi_on.sh");
    nwTimer.start();
}

void QWSPlatform::switchNetworkOff()
{
    runScript("wifi_off.sh");
}

void QWSPlatform::networkActivity()
{
    nwTimer.start();
}

void QWSPlatform::networkTimer()
{
    switchNetworkOff();
}

void QWSPlatform::runScript(const QString &name) const
{
    QString program = "sh";
    QStringList arguments;
    arguments << QString(qgetenv("ROOT")) + QDir::separator() + name;
    QProcess process;
    process.start( program, arguments );
    if ( process.waitForStarted() )
        process.waitForFinished();
}
