#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>

#include <QDir>
#include <QCoreApplication>

#include "QtUtils.h"

#include "KoboPlatform.h"

QString KoboPlatform::getSerialNumber() const
{
    static QString serial;
    if ( serial.isEmpty() )
    {
        QString fileName("/mnt/onboard/.kobo/version");
        serial = ::str_from_file(fileName);
        QStringList tokens( serial.split(',')  );
        serial = tokens.size() >= 1 ? tokens.at(0) : "0";
    }
    return serial;
}

int KoboPlatform::getOrientation() const
{
    static int mapping[4] = {3,0,1,2};
    return mapping[ QWSPlatform::getOrientation() ];
}

void KoboPlatform::setOrientation( int ori )
{
    static int mapping[4] = {1,2,3,0};
    QWSPlatform::setOrientation( mapping[ori] );
}

int KoboPlatform::getBatteryLevel() const
{
    return int_from_file("/sys/devices/platform/pmic_battery.1/power_supply/mc13892_bat/capacity");
}

bool KoboPlatform::isBatteryCharging() const
{
    return str_from_file("/sys/devices/platform/pmic_battery.1/power_supply/mc13892_bat/status") == "Charging";
}

static void to_file( int value )
{
    int light = -1;
    // Open the file for reading and writing
    if ((light = ::open("/dev/ntx_io", O_RDWR)) != -1)
    {
        ::ioctl(light, 241, value);
        ::close(light);
    }

}

bool KoboPlatform::frontlightIsPresent() const
{
    return true;
}

int KoboPlatform::frontlightGetMinLevel() const
{
    return 1;
}

int KoboPlatform::frontlightGetMaxLevel() const
{
    return 100;
}

void KoboPlatform::frontlightSetLevel( int val )
{
    Platform::frontlightSetLevel(val);
    if ( frontlightIsOn() )
    { 
        to_file(frontlightGetLevel());
    }
}

void KoboPlatform::frontlightSetOn( bool val )
{
    Platform::frontlightSetOn(val);
    to_file( frontlightIsOn()? frontlightGetLevel() : 0 );
}

