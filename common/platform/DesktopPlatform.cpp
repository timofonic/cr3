#include <QDir>

#if QT_VERSION >= 0x050000
#include <QStandardPaths>
#else
#include <QDesktopServices>
#include <QDesktopWidget>
#endif
#include <QApplication>
#include <QClipboard>
#include <QLocale>
#include <QScreen>

#include "QtUtils.h"

#include "DesktopPlatform.h"

QString DesktopPlatform::generateSerialNumber() const
{
    QString sn;
#if defined(LINUX)
    sn += "L";
#elif defined(WINDOWS)
    sn += "W";
#elif defined(MACX)
    sn += "M";
#endif
    return sn + Platform::generateSerialNumber();
}

QString DesktopPlatform::getUserSettingsPath() const
{
#if QT_VERSION > 0x050000
    QString path(QStandardPaths::writableLocation(QStandardPaths::DataLocation));
#else
    QString path(QDesktopServices::storageLocation(QDesktopServices::DataLocation));
#endif
    QDir dir(path);
    if (!dir.exists()) dir.mkpath(path);
    return path;
}

QString DesktopPlatform::getRootPath() const
{
#if defined(MACX)
    return QCoreApplication::applicationDirPath() + "/../Resources";
#else
    return QCoreApplication::applicationDirPath();
#endif
}

QString DesktopPlatform::getSystemLang() const
{
    return QLocale::languageToString(QLocale::system().language());
}

qreal DesktopPlatform::getDPI() const
{
#if QT_VERSION > 0x050000
    return QGuiApplication::primaryScreen()->physicalDotsPerInch();
#else
    return ( qApp->desktop()->logicalDpiX() + qApp->desktop()->logicalDpiY() ) / 2.0;
#endif
}

QString DesktopPlatform::getDocumentsPath() const
{
#if QT_VERSION > 0x050000
    QString path(QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation));
#else
    QString path(QDesktopServices::storageLocation(QDesktopServices::DocumentsLocation));
#endif
    path += QDir::separator();
    path += qApp->applicationName();
    QDir dir(path);
    if (!dir.exists()) dir.mkpath(path);
    return path;
}
