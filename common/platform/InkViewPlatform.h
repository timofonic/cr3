#ifndef H__INKVIEW_PLATFORM
#define H__INKVIEW_PLATFORM

#include "QWSPlatform.h"

class InkViewPlatform : public QWSPlatform
{
public:
    virtual QString getSerialNumber() const;
    virtual int getBatteryLevel() const;
    virtual bool isBatteryCharging() const;
    // Frontlight functions
#if defined(OBREEY)
    virtual bool frontlightIsPresent() const;
    virtual int frontlightGetMinLevel() const;
    virtual int frontlightGetMaxLevel() const;
    virtual int frontlightGetLevel() const;
    virtual void frontlightSetLevel( int val );
    virtual bool frontlightIsOn() const;
    virtual void frontlightSetOn( bool val );
#endif
    virtual void setSleepMode( bool val );
#if defined(OBREEY)
    // Network functions (e.g. Wifi, 3G)
    virtual void switchNetworkOn();
    virtual void switchNetworkOff();
#endif
};

#endif // H__INKVIEW_PLATFORM

