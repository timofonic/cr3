#ifndef PLATFORM_H
#define PLATFORM_H

#include <QString>

class Platform
{
    static Platform* p;

protected:
    int frontlightLevel_;
    bool frontlightIsOn_;

protected:
    Platform()
        : frontlightLevel_(0)
        , frontlightIsOn_(false)
    {}

    virtual ~Platform() {}

public:
    static Platform* get();
    static void destroy();
    // License related functions
    virtual QString generateSerialNumber() const;
    virtual QString getSerialNumber() const;
    virtual QString getLicenseKey() const;
    virtual void setLicenseKey( const QString& key ) const;
    // Path functions
    virtual QString getUserSettingsPath() const = 0;
    virtual QString getDocumentsPath() const = 0;
    virtual QString getRootPath() const = 0;
    // Eink functions
    virtual void scheduleEinkFullUpdate() const {}
    // Screen orientation functions
    virtual int getOrientation() const { return 1; }
    virtual void setOrientation( int ) {}
    virtual bool canRotateScreen() const { return false; }
    virtual void lockOrientation() {}
    virtual void unlockOrientation() {}
    // Clipboard functions
    virtual QString getClipboardText() const;
    virtual void setClipboardText( const QString& );
    // Frontlight functions
    virtual bool frontlightIsPresent() const;
    virtual int frontlightGetMinLevel() const;
    virtual int frontlightGetMaxLevel() const;
    virtual int frontlightGetLevel() const;
    virtual void frontlightSetLevel( int val );
    virtual bool frontlightIsOn() const;
    virtual void frontlightSetOn( bool val );
    // Sleep functions
    virtual void setSleepMode( bool val );
    // Translations location
    virtual QString getCommonTranslationsPath() const;
    virtual QString getLocalTranslationsPath() const;
    // Battery functions
    virtual int getBatteryLevel() const;
    virtual bool isBatteryCharging() const;
    // Network functions (e.g. Wifi, 3G)
    virtual bool isNetworkActive() const { return true; }
    virtual void switchNetworkOn() {}
    virtual void switchNetworkOff() {}
    virtual void networkActivity() {}
    // Other functions 
    virtual int getDefaultFontSize() const;
    virtual QString getSystemLang() const;
    virtual qreal getDPI() const = 0;
    virtual void vibrate( int ) {}
    virtual void showAd() {}
    virtual void buyNow() {}
    virtual void rateApp() {}
private:
    QString getLicenseKeyFileName() const;
};

#endif

