#include <QFile>
#include <QProcess>
#include <QStringList>

#include "QtUtils.h"

#include "KindlePlatform.h"

KindlePlatform::KindlePlatform()
    : dev(OTHER)
{
    QString d(qgetenv("DEVICE"));
    if ( d == "KT" )
        dev = KT;
    else
    if ( d == "KT2" )
        dev = KT2; 
    else
    if ( d == "KPW" )
        dev = KPW;
    else
    if ( d == "KPW2" )
        dev = KPW2;
    else
    if ( d == "KV" )
        dev = KV;

    Platform::frontlightSetLevel(::str_from_proc( "/usr/bin/lipc-get-prop com.lab126.powerd flIntensity" ).trimmed().toInt());
}

QString KindlePlatform::getSerialNumber() const
{
    return ::str_from_file("/proc/usid");
}

int KindlePlatform::getBatteryLevel() const
{
    return ::str_from_proc( "/usr/bin/lipc-get-prop com.lab126.powerd battLevel" ).trimmed().toInt();
}

bool KindlePlatform::isBatteryCharging() const
{
    return 1 == ::str_from_proc( "/usr/bin/lipc-get-prop com.lab126.powerd isCharging" ).trimmed().toInt();
}

static const int raw_levels_kpw1[] = {0,2,3,4,5,6,7,8,9,10,13,19,26,36,47,60,75,90,107,126,147,170,196,224,254};
static const int raw_levels_kpw2[] = {0,2,3,4,6,14,28,49,78,120,167,245,336,465,607,775,969,1162,1382,1627,1898,2195,2531,2893,3280,4095};

void KindlePlatform::frontlightStore( int value )
{
    const char* fileName = ( KV == dev || KT2 == dev || KPW2 == dev ) ?
        "/sys/class/backlight/max77696-bl/brightness" :
        "/sys/devices/system/fl_tps6116x/fl_tps6116x0/fl_intensity";
    QFile f(fileName);
    if (f.open(QIODevice::WriteOnly))
    {
         const int* rawLevels = ( KV == dev || KT2 == dev || KPW2 == dev ) ? raw_levels_kpw2:raw_levels_kpw1;
         f.write(QString::number(rawLevels[value]).toAscii());
         f.flush();
    }
}

bool KindlePlatform::frontlightIsPresent() const
{
    return true;
}

int KindlePlatform::frontlightGetMinLevel() const
{
    return 1;
}

int KindlePlatform::frontlightGetMaxLevel() const
{
    return 24;
}

void KindlePlatform::frontlightSetLevel( int val )
{
    Platform::frontlightSetLevel(val);
    if ( frontlightIsOn() )
    {
        frontlightStore( frontlightGetLevel() );
    }
}

void KindlePlatform::frontlightSetOn( bool val )
{
    Platform::frontlightSetOn(val);
    frontlightStore( frontlightIsOn() ? frontlightGetLevel() : 0 );
}

