#ifndef H__KOBO_PLATFORM
#define H__KOBO_PLATFORM

#include "QWSPlatform.h"

class KoboPlatform : public QWSPlatform
{
public:
    virtual QString getSerialNumber() const;
    virtual int getOrientation() const;
    virtual void setOrientation( int ori );
    virtual int getBatteryLevel() const;
    virtual bool isBatteryCharging() const;
    virtual bool frontlightIsPresent() const;
    virtual int frontlightGetMinLevel() const;
    virtual int frontlightGetMaxLevel() const;
    virtual void frontlightSetLevel( int val );
    virtual void frontlightSetOn( bool val );
};

#endif // H__KOBO_PLATFORM
