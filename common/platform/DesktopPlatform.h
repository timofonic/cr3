#include "Platform.h"

class DesktopPlatform : public Platform
{
public:
    virtual QString generateSerialNumber() const;
    virtual QString getUserSettingsPath() const;
    virtual QString getDocumentsPath() const;
    virtual QString getRootPath() const;
    virtual int getOrientatation() const { return 1; }
    virtual void setOrientation( int ) {}
    virtual QString getSystemLang() const;
    virtual qreal getDPI() const;
};
