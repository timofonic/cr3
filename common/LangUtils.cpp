#include <QApplication>
#include <QTranslator>

#include "Config.h"
#include "Factory.h"
#include "LanguageDescriptor.h"

QTranslator* g_pTrans1;
QTranslator* g_pTrans2;

int getLangIndex()
{
    // translations
    Factory<LanguageDescriptor>& ldf = *Factory<LanguageDescriptor>::instance();

    int index = 0;
    int lang = g_pConfig->readInt("lang", 0);

    if ( 0 == lang ) // system
    {
        // try to use system language
        QString langName = Platform::get()->getSystemLang();
        if ( !langName.isEmpty() )
        {
            Factory<LanguageDescriptor>::Iterator i( ldf.begin() );
            for ( ; i != ldf.end() && langName != i->name; i++, index++ )
            {}
            if ( i == ldf.end() )
                index = 1; // Not found, fallback to English
        }
        else
        {
            index = 1; // English
        }
    }
    else
    {
        index = lang;  // Manual language selection
    }

    return index;
}

QString getLangName()
{
    int lang = g_pConfig->readInt( "lang", -1 );
    QString locale;
    if ( lang >= 0 )
    {
        Factory<LanguageDescriptor>& ldf = *Factory<LanguageDescriptor>::instance();
        locale = ldf.at(lang).name;
    }
    else
    {
        locale = Platform::get()->getSystemLang();
    }
    return locale;
}

QString getLangForWebLink()
{
    QString lang(::getLangName());
    return (lang == "Russian" || lang == "Ukrainian") ? "ru" : "en";
}

void loadTranslations()
{
    // translations
    Factory<LanguageDescriptor>& ldf = *Factory<LanguageDescriptor>::instance();

    if ( ldf.isEmpty() )
    {
        QString path = Platform::get()->getCommonTranslationsPath() + QDir::separator() + "languages.txt";
        ldf.init( path );
    }

    int index = getLangIndex();

    if ( !g_pTrans1)
    {
        g_pTrans1 = new QTranslator(qApp);
        qApp->installTranslator(g_pTrans1);
    }
    g_pTrans1->load(ldf.at(index).getTrans1FileName());

    if ( !g_pTrans2 )
    {
        g_pTrans2 = new QTranslator(qApp);
        qApp->installTranslator(g_pTrans2);
    }
    g_pTrans2->load(ldf.at(index).getTrans2FileName());
}

void cleanupTranslations()
{
    Factory<LanguageDescriptor>::destroy();
}

QString trans(const char *ctx, const char *text)
{
    return qApp->translate(ctx, text);
}

