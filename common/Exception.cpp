#include <cstdio>

#include "Exception.h"

Exception::Exception(bool isCritical, const std::string& message)
    : isCritical_(isCritical)
    , message_(message)
{
}


Exception::Exception(bool isCritical, const char* pFormat, ...)
    : isCritical_(isCritical)
{
    va_list ap;

    va_start(ap, pFormat);
    formatMessage(pFormat, ap);
    va_end(ap);
}


Exception::~Exception() throw()
{
}

void Exception::formatMessage(const char* pFormat, ...)
{
    va_list ap;

    va_start(ap, pFormat);
    formatMessage(pFormat, ap);
    va_end(ap);
}

void Exception::formatMessage(const char* pFormat, va_list ap)
{
    const int buffLen = 8192;
    char pBuff[buffLen];

    vsnprintf(pBuff, buffLen, pFormat, ap);
    pBuff[buffLen - 1] = '\0';
    message_ = pBuff;
}

const char* Exception::what(void) const throw()
{
    return message_.c_str();
}

bool Exception::isCritical() const
{
    return isCritical_;
}
