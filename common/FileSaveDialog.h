#ifndef FILESAVEDIALOG_H_
#define FILESAVEDIALOG_H_

#include <QFileSystemModel>
#include <QTreeView>

#include "Dialog.h"

#include "ui_FileSaveDialog.h"

class FileSaveDialog: public Dialog, private Ui::FileSaveDialog
{
    Q_OBJECT

    QFileSystemModel fsModel;
    QString fileName;
    QString fileExt;
    QString defaultSuffix;

public:
    FileSaveDialog(QWidget* parent, const QString& path, const QString& ext);

    const QString& getFileName() const
    { return fileName; }

    void setFileName( const QString& fn );

    void setDefaultSuffix( const QString& defSuff )
    { defaultSuffix = defSuff; }

    QString getDefaultSuffix() const
    { return defaultSuffix; }

    void setModelFilters( QDir::Filters val )
    { fsModel.setFilter(val); }

    QDir::Filters modelFilters() const
    { return fsModel.filter(); }

private:
    virtual void keyPressEvent(QKeyEvent* e);

private slots:
    void select( const QModelIndex & index );
    void accept();
    void onBtnUp();
};

#endif /* FILESAVEDIALOG_H_ */

