#ifndef H__LANGUAGE_DESCRIPTOR
#define H__LANGUAGE_DESCRIPTOR

#include <QVector>
#include <QString>

class LanguageDescriptor
{
public:
    QString name;
    QString native;
    QString trans1;
    QString trans2;
    QString kbd;

public:
    LanguageDescriptor();
    LanguageDescriptor( const QString& str );

    QString getTrans1FileName() const;
    QString getTrans2FileName() const;
    QString getKbdFileName() const;
};

#endif

