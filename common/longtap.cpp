#include "longtap.h"

#include <QMouseEvent>
#include <QDebug>

LongTap::LongTap(int interval, QObject *parent)
    : QObject(parent)
{
    timer.setInterval(interval);
    timer.setSingleShot(true);
    QObject::connect( &timer, SIGNAL(timeout()), this, SLOT(timerSlot()) );
}

LongTap::~LongTap()
{
}

bool LongTap::eventFilter(QObject *, QEvent *event)
{
    int type = event->type();
    if (    QEvent::MouseButtonPress   == type
         || QEvent::MouseMove          == type
         || QEvent::MouseButtonRelease == type )
    {
        QMouseEvent* e = static_cast<QMouseEvent*>(event);

        if (    QEvent::MouseMove == type
             && e->buttons() != Qt::LeftButton )
            return false;

        if ( e->modifiers() != Qt::NoModifier )
            return false;

        if (    QEvent::MouseMove == type
             || Qt::LeftButton == e->button() )
        {
            switch ( event->type() )
            {
            case QEvent::MouseButtonPress:
                pt = e->pos();
                timer.start();
                //qDebug("timer.start(press)");
                break;
            case QEvent::MouseMove:
                pt = e->pos();
                timer.start();
                //qDebug("timer.start(move)");
                break;
            case QEvent::MouseButtonRelease:
                timer.stop();
                //qDebug("timer.stop(release)");
                break;
            default:
                break;
            }
        }
    }
    return false;
}

void LongTap::timerSlot()
{
    emit longTapSignal(pt);
}
