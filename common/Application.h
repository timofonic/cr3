#ifndef APPLICATION_H_
#define APPLICATION_H_

#include <QApplication>
#include <QDebug>

#if defined(ANDROID)
#include "AndroidKeyFilter.h"
#endif

class Application : public QApplication
{
    Q_OBJECT
#if defined(ANDROID)
    AndroidKeyFilter kf;
#endif

public:
    Application( int & argc, char ** argv );
    ~Application();

    bool init();

protected:
    virtual void customInit() {}

#if defined(ANDROID) || defined(SIMPLE) || defined(IOS)
protected slots:
    virtual void stateChanged( Qt::ApplicationState state ); 
#endif
};

#endif /* APPLICATION_H_ */
