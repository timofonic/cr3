#ifndef DICTIONARYWIDGET_H
#define DICTIONARYWIDGET_H

#include <QPlainTextEdit>
#include <QProcess>

#include "gesturescontroller.h"

class DictionaryWidget : public QPlainTextEdit
{
    Q_OBJECT

    QProcess sdcv;
    GesturesController gc;

public:
    DictionaryWidget(QWidget *parent = 0);

    void translate( const QString& phrase );

private slots:
    void onProcessReadyReadStdOutput();
    void onProcessReadyReadStdError();
    void onProcessFinished(int);
    void onGesture(QPoint, GesturesController::GestureType t);

signals:
    void signalTranslateFinish();
};

#endif // DICTIONARYWIDGET_H
