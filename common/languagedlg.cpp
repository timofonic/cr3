#include "Factory.h"
#include "Config.h"
#include "LangUtils.h"
#include "LanguageDescriptor.h"
#include "languagedlg.h"
#include "ui_languagedlg.h"

static const char* sysStr = QT_TR_NOOP("System");

LanguageDlg::LanguageDlg(QWidget *parent)
    : Dialog(parent)
    , ui(new Ui::LanguageDlg)
{
    ui->setupUi(this);
    init();

    const Factory<LanguageDescriptor>& ldf = *Factory<LanguageDescriptor>::instance();
    for ( int i=0; i<ldf.size(); ++i )
    {
        ui->lwLanguage->addItem(0==i? tr(sysStr):ldf[i].native);
    }
    ui->lwLanguage->setCurrentRow(g_pConfig->readInt("lang", 0));

    QObject::connect(ui->lwLanguage, SIGNAL(itemActivated(QListWidgetItem*)), this, SLOT(itemActivated(QListWidgetItem*)));

#if defined(QT_KEYPAD_NAVIGATION)
    ui->lwLanguage->setEditFocus(true);
#endif
}

LanguageDlg::~LanguageDlg()
{
    delete ui;
}

void LanguageDlg::accept()
{
    Dialog::accept();
    int lang = ui->lwLanguage->currentRow();
    g_pConfig->writeInt("lang", lang);
    loadTranslations();
}

void LanguageDlg::itemActivated(QListWidgetItem *item)
{
    if ( item != NULL )
        accept();
}
