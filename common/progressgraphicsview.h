#ifndef PROGRESSGRAPHICSVIEW_H
#define PROGRESSGRAPHICSVIEW_H

#include "graphicsview.h"

class ProgressGraphicsView : public GraphicsView
{
    QSize size;

public:
    ProgressGraphicsView(QWidget* parent = 0);

    void setSizeHint( const QSize& size_ )
    { size = size_; }

    QSize sizeHint() const
    { return size; }
};

#endif // PROGRESSGRAPHICSVIEW_H
