#include "graphicsview.h"

GraphicsView::GraphicsView(QWidget* parent)
    : QGraphicsView(parent)
{
}

void GraphicsView::fitScene()
{
    if ( scene() )
    {
        fitInView( scene()->sceneRect(), Qt::KeepAspectRatio);
    }
}

void GraphicsView::resizeEvent(QResizeEvent*)
{
    fitScene();
}
