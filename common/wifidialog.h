#ifndef WIFIDIALOG_H
#define WIFIDIALOG_H

#include <QThread>

#include "Dialog.h"

namespace Ui {
class WifiDialog;
}

class WifiDialog : public Dialog
{
    Q_OBJECT

public:
    WifiDialog(QWidget *parent = 0);
    ~WifiDialog();

private slots:
    void on_btnCancel_clicked();

private:
    Ui::WifiDialog *ui;
    QThread* wifiThread;
};

#endif // WIFIDIALOG_H
