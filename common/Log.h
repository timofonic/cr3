#include <fstream>
#include <string>

using std::ofstream;

class Log
{
    std::ofstream f;
    int level;
    int dupLevel;
    std::string name;

public:
    enum MsgType
    {
        MT_I,  // Informational message
        MT_W,  // Warning message
        MT_E,  // Non-critical error message
        MT_F   // fatal error message
    };

public:
    Log( const char* nm, int lvl, int dlvl );

    void write( int lvl, MsgType type, const char* format, ... ) __attribute__((format(printf, 4, 5)));

    int getLevel() const 
    { return level; }

    void setLevel( int lvl )
    { level = lvl; }

    void setDupLevel( int lvl )
    { dupLevel = lvl; }

private:
    void getCurrentTime( char str[20] ) const; // "YYYY-MM-DD HH:MI:SS"
};

extern Log* g_pLog;

