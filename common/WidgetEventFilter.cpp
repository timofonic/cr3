#include <QEvent>
#include <QKeyEvent>
#include <QLineEdit>
#include <QSpinBox>
#include <QDoubleSpinBox>
#include <QListView>
#include <QTreeView>
#include <QTextEdit>
#include <QPushButton>
#include <QToolButton>
#include <QCheckBox>
#include <QComboBox>
#include <QPlainTextEdit>
#include <QCoreApplication>
#include <QDebug>

#if defined(Q_WS_QWS)
 #include "VirtualKeyboardContainer.h"
#endif

#include "WidgetEventFilter.h"

WidgetEventFilter::WidgetEventFilter(QWidget* t, VirtualKeyboardContainer* v)
    : tlw(t)
    , vkc(v)
{}

bool WidgetEventFilter::filter(QObject *obj, QEvent *event)
{
    QWidget* w = qobject_cast<QWidget*>(obj);

    switch ( event->type() )
    {
#if defined (Q_WS_QWS)
    case QEvent::EnterEditFocus:
        if (    qobject_cast<QLineEdit*>(obj)
             || qobject_cast<QSpinBox*>(obj)
             || qobject_cast<QDoubleSpinBox*>(obj) )
        {
            w->update();
            vkc->showVirtualKeyboard( qobject_cast<QWidget*>(obj) );
        }
        else
        if (    qobject_cast<QPlainTextEdit*>(obj)
             || qobject_cast<QTextEdit*>(obj) )
        {
            vkc->getVirtualKeyboard()->setBuddy( qobject_cast<QWidget*>(obj) );
        }
        else
        if ( qobject_cast<QAbstractItemView*>(obj) )
        {
            w->update();
        }
        break;

    case QEvent::LeaveEditFocus:
        if (    qobject_cast<QLineEdit*>(obj)
             || qobject_cast<QSpinBox*>(obj)
             || qobject_cast<QDoubleSpinBox*>(obj) )
        {
            w->update();
            vkc->showVirtualKeyboard( NULL );
        }
        else
        if (    qobject_cast<QPlainTextEdit*>(obj)
             || qobject_cast<QTextEdit*>(obj) )
        {
            vkc->getVirtualKeyboard()->setBuddy( qobject_cast<QWidget*>(obj) );
        }
        else
        if ( qobject_cast<QAbstractItemView*>(obj) )
        {
            w->update();
        }
        break;

    case QEvent::KeyPress:
    case QEvent::KeyRelease:
        {
            QKeyEvent* e = static_cast<QKeyEvent*>(event);
            if ( w->hasEditFocus() )
            {
                if ( Qt::Key_Back == e->key() )
                {
                    if ( QEvent::KeyRelease == e->type() )
                    {
                        w->setEditFocus(false);
                    }
                    e->accept();
                    return true;
                }
                VirtualKeyboard* vk = vkc->getVirtualKeyboard();
                if (    vk
                     && e->spontaneous()
                     && (    qobject_cast<QLineEdit*>(obj)
                          || qobject_cast<QSpinBox*>(obj)
                          || qobject_cast<QDoubleSpinBox*>(obj)
                          || qobject_cast<QTextEdit*>(obj)
                          || qobject_cast<QPlainTextEdit*>(obj) ) )
                {
                    switch ( e->key() )
                    {
                    case Qt::Key_Home:
                        if ( QEvent::KeyRelease == e->type() )
                        {
                            vk->changeLayout();
                        }
                        e->accept();
                        return true;
                    case Qt::Key_Menu:
                        if ( QEvent::KeyRelease == e->type() )
                        {
                            vk->setProcessCursorKeys( !vk->getIsProcessCursorKeys() );
                        }
                        e->accept();
                        return true;
                    default:
                        if (    e->key() != Qt::Key_Return
                             && e->key() != Qt::Key_Delete )
                        {
                            // re-direct some keyboard events to the virtual keyboard
                            // for QLineEdit, QTextEdit, QSpinBox, QDoubleSpinBox if virtual keyboard exists
                            QCoreApplication::postEvent( vk, new QKeyEvent( e->type(), e->key(), e->modifiers(), e->text(), e->isAutoRepeat(), e->count() ) );
                            e->accept();
                            return true;
                        }
                        break;
                    }
                }
            }
        }
        break;
#endif

    default:
        break;
    }
    return false;
}
