#ifndef PROGRESSSCENE_H
#define PROGRESSSCENE_H

#include <QTimer>
#include <QGraphicsScene>

class QGraphicsRectItem;

class ProgressScene : public QGraphicsScene
{
Q_OBJECT

    QGraphicsRectItem* pItem[8];
    QTimer timer;
    int stage;

public:
    ProgressScene(QObject* parent = NULL);

private slots:
    void onTimer();
};

#endif // PROGRESSSCENE_H
