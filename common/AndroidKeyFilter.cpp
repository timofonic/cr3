#include <QKeyEvent>
#include <QCoreApplication>

#include "AndroidKeyFilter.h"

bool AndroidKeyFilter::eventFilter(QObject* obj, QEvent* event)
{
     if (    QEvent::KeyPress   == event->type()
          || QEvent::KeyRelease == event->type() )
     {
         QKeyEvent* kevent = static_cast<QKeyEvent*>(event);
         if ( Qt::Key_Enter == kevent->key() )
         {
             QCoreApplication::postEvent( obj, new QKeyEvent( kevent->type(), Qt::Key_Select, kevent->modifiers(),
                                                              kevent->text(), kevent->isAutoRepeat(), kevent->count() ) );
             return true;
         }
         if ( Qt::Key_PageDown == kevent->key() )
         {
             QCoreApplication::postEvent( obj, new QKeyEvent( kevent->type(), Qt::Key_Tab, kevent->modifiers(),
                                                              kevent->text(), kevent->isAutoRepeat(), kevent->count() ) );
             return true;
         }
     }
     return false;
}

