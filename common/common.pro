TEMPLATE  = lib
CONFIG   += lib
kobo|kindle|onyx|pb|obreey|qvfb|android_armv5|android_armv7|android_x86|simple {
    VERSION =
    CONFIG += dll plugin
}
else {
    CONFIG += staticlib
}

include(common.pri)
!gpl {
    include (license.pri)
}
android {
    QT += androidextras
}
SOURCES += utils.cpp Log.cpp Exception.cpp QtUtils.cpp Dialog.cpp Application.cpp EnterTextLineDialog.cpp Config.cpp Platform.cpp statemachine.cpp LangUtils.cpp LanguageDescriptor.cpp \
    languagedlg.cpp \
    graphicsview.cpp \
    progressgraphicsview.cpp
!desktop {
    SOURCES += FileOpenDialog.cpp FileSaveDialog.cpp StyleEbook.cpp gesturescontroller.cpp rotationdlg.cpp menufilter.cpp
    simple|android|ios {
        SOURCES += AndroidKeyFilter.cpp
    }
    else {
        SOURCES += VirtualKeyboard.cpp WidgetEventFilter.cpp DictionaryWidget.cpp VirtualKeyboardContainer.cpp
    }
    eink|qvfb {
        SOURCES += pipethread.cpp wifidialog.cpp progressscene.cpp
    }
}
HEADERS += utils.h Log.h Exception.h QtUtils.h Dialog.h version.h Application.h EnterTextLineDialog.h Config.h Platform.h statemachine.h LangUtils.h LanguageDescriptor.h \
    languagedlg.h \
    graphicsview.h \
    progressgraphicsview.h
!desktop {
    HEADERS += FileOpenDialog.h FileSaveDialog.h StyleEbook.h gesturescontroller.h rotationdlg.h menufilter.h
    simple|android|ios {
        HEADERS += AndroidKeyFilter.h
    }
    else {
        HEADERS += VirtualKeyboard.h WidgetEventFilter.h DictionaryWidget.h VirtualKeyboardContainer.h
    }
    eink|qvfb {
        HEADERS += pipethread.h wifidialog.h progressscene.h
    }
}
FORMS   += TreeViewDialog.ui EnterTextLineDialog.ui \ 
    languagedlg.ui \
    wifidialog.ui
!desktop {
    FORMS += rotationdlg.ui FileOpenDialog.ui FileSaveDialog.ui
}

!gpl {
    revision.target = revision.h
    revision.commands = sh revision.sh
    revision.clean = revision.h
    revision.depends = ../.hg
    QMAKE_EXTRA_TARGETS += revision
    PRE_TARGETDEPS += revision.h
}

