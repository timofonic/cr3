#include "QtUtils.h"

#include "EnterTextLineDialog.h"

EnterTextLineDialog::EnterTextLineDialog(QWidget* parent, const QString& title, const QString& prompt, const QString& value)
    : Dialog(parent)
{
    setupUi(this);
    init();
    setWindowTitle(title);
    lblPrompt->setText(prompt);
    leValue->setText(value);
#if defined(QT_KEYPAD_NAVIGATION)
    QTimer::singleShot(0, this, SLOT(onSelectText()));
#endif
}

#if defined(QT_KEYPAD_NAVIGATION)
void EnterTextLineDialog::onSelectText()
{
    leValue->setEditFocus(true);
    leValue->selectAll();
}
#endif
