#include <QKeyEvent>

#include "QtUtils.h"

#include "FileSaveDialog.h"

FileSaveDialog::FileSaveDialog(QWidget* parent, const QString& path, const QString& ext)
    : Dialog(parent)
    , fileExt(ext)
{
    setupUi(this);
    init();

    QStringList filters;
    filters.append(QString("*") + ext);
    fsModel.setNameFilters(filters);
    fsModel.setNameFilterDisables(false);
    fsModel.setRootPath("/");
    treeView->setModel(&fsModel);
    treeView->setRootIndex(fsModel.index(path));
    treeView->setHeaderHidden(true);
    treeView->setColumnHidden(1,true);
    treeView->setColumnHidden(2,true);
    treeView->setColumnHidden(3,true);

    connect(treeView, SIGNAL(activated(QModelIndex)), this, SLOT(select(QModelIndex)));
    connect(treeView, SIGNAL(clicked(QModelIndex)), this, SLOT(select(QModelIndex)));
}

void FileSaveDialog::setFileName(const QString &fn)
{
    lineEdit->setText( fn );
}

void FileSaveDialog::keyPressEvent(QKeyEvent* e)
{
    switch( e->key() )
    {
    case Qt::Key_Menu:
        btnUp->setFocus();
        break;
    default:
        break;
    }
}

void FileSaveDialog::select( const QModelIndex& index )
{
    if ( index.isValid() )
    {
        if ( fsModel.hasChildren( index ) )
        {
            treeView->setExpanded( index, !treeView->isExpanded( index ) );
        }
        else
        {
            QFileInfo fInfo = fsModel.fileInfo(index);
            if ( fInfo.isFile() )
            {
                lineEdit->setText( fInfo.fileName() );
            }
        }
    }
}

void FileSaveDialog::accept()
{
    if ( lineEdit->text().isEmpty() ) return;

    QModelIndex index(treeView->selectionModel()->currentIndex());

    if ( !index.isValid() )
        index = treeView->rootIndex();

    QFileInfo fInfo = fsModel.fileInfo(index);
    if ( fInfo.isFile() )
    {
        fileName = fInfo.absolutePath() + QDir::separator() + lineEdit->text();
    }
    else
    if ( fInfo.isDir() )
    {
        fileName = fInfo.absoluteFilePath() + QDir::separator() + lineEdit->text();
    }

    if ( !defaultSuffix.isEmpty() && QFileInfo(fileName).suffix() != defaultSuffix )
        fileName += "." + defaultSuffix;

    bool isFile = QFileInfo( fileName ).isFile();
    if ( isFile )
    {        
        QMessageBox::StandardButton result = ::questionBox( this, tr("Save"), tr("File exists. Overwrite?"), QMessageBox::Yes|QMessageBox::No, QMessageBox::No );
        if ( QMessageBox::Yes == result )
            Dialog::accept();
        else
            Dialog::reject();
    }
    else
    {
        Dialog::accept();
    }
}

void FileSaveDialog::onBtnUp()
{
    QModelIndex oldRoot(treeView->rootIndex());
    QModelIndex newRoot(fsModel.parent(oldRoot));
    if ( newRoot.isValid() )
    {
        treeView->setRootIndex( newRoot );
        treeView->setCurrentIndex( oldRoot );
    }
}
