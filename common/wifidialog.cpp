#include "Platform.h"
#include "progressscene.h"

#include "wifidialog.h"
#include "ui_wifidialog.h"

class WiFiThread: public QThread
{
public:
    WiFiThread( QObject* parent = NULL )
        : QThread(parent)
    {
    }

    virtual void run()
    {
        Platform::get()->switchNetworkOn();
        int counter = 20;
        while ( !Platform::get()->isNetworkActive() && counter >= 0 )
        {
            counter--;
            QThread::sleep(1);
        }

        // sleep 3 sec to activate network
        for ( int i=0; i<3; ++i )
            QThread::sleep(1);
    }
};


WifiDialog::WifiDialog(QWidget *parent)
    : Dialog(parent)
    , ui(new Ui::WifiDialog)
    , wifiThread(new WiFiThread(this))
{
    setWindowFlags( windowFlags() | Qt::FramelessWindowHint );
    setFullScreen(false);
    ui->setupUi(this);    
    init();

    connect( wifiThread, SIGNAL(finished()), this, SLOT(accept()) );

    ui->graphicsView->setScene(new ProgressScene(this));

    wifiThread->start();

    adjustSize();
}

WifiDialog::~WifiDialog()
{
    delete ui;
}

void WifiDialog::on_btnCancel_clicked()
{
    if ( wifiThread->isRunning() )
        wifiThread->terminate();
    reject();
}
