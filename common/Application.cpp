#include <QFont>
#include <QDir>
#include <QPalette>
#if defined(Q_WS_QWS)
 #include<QWSServer>
#endif
#if !defined(DESKTOP)
 #include "StyleEbook.h"
#endif

#include "Platform.h"
#include "Exception.h"
#include "QtUtils.h"
#include "version.h"
#include "utils.h"
#include "Log.h"
#include "Config.h"
#include "Platform.h"
#include "LangUtils.h"

#include "Application.h"

Application::Application( int& argc, char** argv )
    : QApplication(argc, argv)
{
#if defined(ANDROID) || defined(SIMPLE) || defined(IOS)
    QObject::connect( this, SIGNAL(applicationStateChanged(Qt::ApplicationState)), this, SLOT(stateChanged(Qt::ApplicationState)) );
#endif
}

Application::~Application()
{
    cleanupTranslations();

    if ( g_pLog != NULL )
        g_pLog->write(1, Log::MT_I, "Program finished");

    delete g_pConfig;
    delete g_pLog;

    Platform::destroy();
}

bool Application::init()
try
{
    // initialize resources
#if !defined(DESKTOP)
    Q_INIT_RESOURCE(keyboard);
    Q_INIT_RESOURCE(rotation);
    Q_INIT_RESOURCE(eink);
#if defined(EINK)
    setCursorFlashTime(0);
#endif
#endif

    QString logFile( Platform::get()->getUserSettingsPath() + QDir::separator() + applicationName() + ".log.txt" );
    g_pLog = new Log( QFile::encodeName(logFile).constData(), 1, 1 );

    g_pConfig = new Config(applicationName());
    int logLevel = g_pConfig->readInt("log_level", -1);
    if ( -1 == logLevel )
    {
        logLevel = qgetenv("VLASOVSOFT_LOG").toInt();
    } 
    if ( logLevel < 0 ) logLevel = 0;
    if ( logLevel > 2 ) logLevel = 2;
    g_pLog->setLevel( logLevel );

    g_pLog->write(1, Log::MT_I, "Program started, version: %s.%s", VERSION, REVISION);

    // set default application font
    QFont fnt = font();
    fnt.setPointSize(Platform::get()->getDefaultFontSize());
    setFont(fnt);

#if defined(EINK)
    // set palette
    QPalette pal = palette();
    pal.setColor(QPalette::Window, Qt::white);
    pal.setColor(QPalette::Highlight, Qt::black);
    setPalette(pal);
#endif

#if defined(ANDROID) || defined(SIMPLE) || defined(IOS)
    QPalette pal = palette();
    pal.setColor(QPalette::Highlight, Qt::darkBlue);
    pal.setColor(QPalette::HighlightedText, Qt::white);
    setPalette(pal);
#endif

#if !defined(DESKTOP)
    StyleEbook* pStyle = new StyleEbook(style());
    int bSize = g_pConfig->readInt( "button_size", 40 ); // 40 = 1 cm
    QSize size( getSizePx( QSize(bSize, bSize), 5 ) );
    pStyle->setButtonIconSize(qMax( size.width(), size.height() ));
    setStyle(pStyle);
#endif

#if defined(Q_WS_QWS)
    QWSServer::setBackground(QBrush(Qt::black));
    setNavigationMode( Qt::NavigationModeKeypadDirectional );
#endif

#if defined(ANDROID)
    installEventFilter(&kf);
#endif

    loadTranslations();

    customInit();

    return true;
}
catch(const Exception& e)
{
    if ( g_pLog != NULL )
        g_pLog->write(1, Log::MT_F, "Fatal error: %s", e.what());
    ::messageBox(0, QMessageBox::Critical, qApp->translate("main", "Error:"), QString::fromLocal8Bit(e.what()) );
    return false;
}
catch(...)
{
    if ( g_pLog != NULL )
        g_pLog->write(1, Log::MT_F, "Fatal error!");
    ::messageBox(0, QMessageBox::Critical, qApp->translate("main", "Error:"), "Fatal error!" );
    return false;
}

#if defined(ANDROID) || defined(SIMPLE) || defined(IOS)
void Application::stateChanged( Qt::ApplicationState )
{
}
#endif

