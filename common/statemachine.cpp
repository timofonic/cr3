#include "statemachine.h"

StateMachine::StateMachine( int state, StateMachineCallback* pCallback )
    : state_(state)
    , pCallback_(pCallback)
{
}

void StateMachine::event(int event)
{
    for ( int i=0; i<transitions.size(); ++i )
    {
        Transition& t = transitions[i];
        if ( t.oldState == state_ && t.event == event )
        {
            state_ = t.newState;
            if ( pCallback_ )
                pCallback_->onStateChanged( t );
            break;
        }
    }
}
