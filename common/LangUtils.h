#ifndef LANGUTILS_H
#define LANGUTILS_H

#include <QString>

int getLangIndex();
QString getLangName();
QString getLangForWebLink();
void loadTranslations();
void cleanupTranslations();
QString trans(const char* ctx, const char* text);

#endif // LANGUTILS_H
