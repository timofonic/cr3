#include <ctime>
#include <cstdio>
#include <cstdarg>

#include <fstream>

#include "Log.h"

Log* g_pLog;

Log::Log( const char* nm, int lvl, int dlvl )
    : level(lvl)
    , dupLevel(dlvl)
    , name(nm)
{
}

void Log::write( int lvl, Log::MsgType type, const char* format, ... )
{
    if ( lvl <= level )
    {
        char t[5] = "IWEF";
        char pBuff[1024];
        char time[20];
        unsigned buffLen = sizeof(pBuff)/sizeof(pBuff[0]);
        if ( !f.is_open() )
        {
            f.open( name.c_str(), std::ios_base::out | std::ios_base::app );
        }
        if ( f.good() )
        {
            va_list ap;
            va_start(ap, format);
            vsnprintf(pBuff, buffLen, format, ap);
            pBuff[buffLen - 1] = '\0';
            va_end(ap);
            getCurrentTime( time );
            f << '[' << time << "]\t" << t[type] << '\t' << pBuff << std::endl;
            f.flush();
        }
        if ( lvl <= dupLevel )
        {
            printf("[%s]\t%c\t%s\n", time, t[type], pBuff);
            fflush(stdout);
        }
    }
}

void Log::getCurrentTime( char str[20] ) const
{
    time_t t = time(NULL);
    const tm* ptm = localtime(&t);
    snprintf( str, 20, "%04d-%02d-%02d %02d:%02d:%02d", 
              1900+ptm->tm_year, 1+ptm->tm_mon, ptm->tm_mday, 
              ptm->tm_hour, ptm->tm_min, ptm->tm_sec );
}

