#ifndef ANDROIDKEYFILTER_H_
#define ANDROIDKEYFILTER_H_

#include <QObject>

class AndroidKeyFilter : public QObject
{
protected:
     bool eventFilter(QObject *obj, QEvent *event);
};

#endif /* ANDROIDKEYFILTER_H_ */

