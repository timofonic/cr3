#include <QDialogButtonBox>
#include <QVBoxLayout>
#include <QKeyEvent>

#include "FileOpenDialog.h"

FileOpenDialog::FileOpenDialog(QWidget* parent, const QString& path, const QString& exts, bool hidden)
    : Dialog(parent)
{
    setupUi(this);
    init();

    if ( hidden ) fsModel.setFilter(fsModel.filter() | QDir::Hidden);
    QStringList filters;
    filters = exts.split("|", QString::SkipEmptyParts);
    fsModel.setNameFilters(filters);
    fsModel.setNameFilterDisables(false);
    fsModel.setRootPath("/");
    treeView->setModel(&fsModel);
    treeView->setRootIndex(fsModel.index(path));
    treeView->setHeaderHidden(true);
    treeView->setColumnHidden(1,true);
    treeView->setColumnHidden(2,true);
    treeView->setColumnHidden(3,true);

    QObject::connect(treeView, SIGNAL(activated(QModelIndex)), this, SLOT(select(QModelIndex)) );
    QObject::connect(treeView, SIGNAL(clicked(QModelIndex)), this, SLOT(expand(QModelIndex)) );

#ifdef QT_KEYPAD_NAVIGATION
    treeView->setEditFocus(true);
#endif
}

void FileOpenDialog::keyPressEvent(QKeyEvent *e)
{
    switch( e->key() )
    {
    case Qt::Key_Menu:
        btnUp->setFocus();
        break;
    default:
        break;
    }
}

void FileOpenDialog::accept()
{
    QModelIndex index = treeView->selectionModel()->currentIndex();
    select( index );
}

void FileOpenDialog::onBtnUp()
{
    QModelIndex oldRoot(treeView->rootIndex());
    QModelIndex newRoot(fsModel.parent(oldRoot));
    if ( newRoot.isValid() )
    {
        treeView->setRootIndex( newRoot );
        treeView->setCurrentIndex( oldRoot );
    }
}

void FileOpenDialog::select( const QModelIndex& index )
{
    if ( index.isValid() )
    {
        if ( fsModel.hasChildren( index ) )
        {
            treeView->setExpanded( index, !treeView->isExpanded( index ) );
        }
        else
        {
            QFileInfo fInfo = fsModel.fileInfo(index);
            if ( fInfo.isFile() )
            {
                fileName = fInfo.absoluteFilePath();
                QDialog::accept();
            }
        }
    }
}

void FileOpenDialog::expand(const QModelIndex &index)
{
    if ( index.isValid() )
    {
        if ( fsModel.hasChildren( index ) )
            treeView->setExpanded( index, !treeView->isExpanded( index ) );
    }
}
