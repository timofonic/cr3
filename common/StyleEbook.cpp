#include <QPainter>
#include <QStyleOption>
#include <QApplication>
#include <QDebug>

#include "StyleEbook.h"

#include <QPainter>
#include <QStyleOption>
#include <QApplication>
#include <QDebug>

#include "StyleEbook.h"

StyleEbook::StyleEbook( QStyle* style )
    : QProxyStyle(style)
    , scrollBarSize(5)
    , buttonIconSize(48)
{}

QRect StyleEbook::subControlRect ( ComplexControl control, const QStyleOptionComplex* option, SubControl subControl, const QWidget * widget ) const
{
    if ( QStyle::CC_ScrollBar == control )
    {
        QRect ret;
        if ( const QStyleOptionSlider *scrollbar = qstyleoption_cast<const QStyleOptionSlider *>(option) )
        {
            if (    subControl != SC_ScrollBarSubPage
                 && subControl != SC_ScrollBarAddPage
                 && subControl != SC_ScrollBarSlider )
                return ret;

            const QRect scrollBarRect = scrollbar->rect;
            int maxlen = (scrollbar->orientation == Qt::Horizontal) ?
                scrollBarRect.width() : scrollBarRect.height();

            // calculate slider length
            int sliderlen;
            if (scrollbar->maximum != scrollbar->minimum)
            {
                uint range = scrollbar->maximum - scrollbar->minimum;
                sliderlen = (qint64(scrollbar->pageStep) * maxlen) / (range + scrollbar->pageStep);
                int slidermin = proxy()->pixelMetric(PM_ScrollBarSliderMin, scrollbar, widget);
                if (sliderlen < slidermin || range > INT_MAX / 2)
                    sliderlen = slidermin;
                if (sliderlen > maxlen)
                    sliderlen = maxlen;
            }
            else
            {
                sliderlen = maxlen;
            }
            // calculate slider start
            int sliderstart = sliderPositionFromValue(scrollbar->minimum,
                                                      scrollbar->maximum,
                                                      scrollbar->sliderPosition,
                                                      maxlen - sliderlen,
                                                      scrollbar->upsideDown);
            // calculate slider length
            switch( subControl )
            {
            case SC_ScrollBarSubPage:
                if (scrollbar->orientation == Qt::Horizontal)
                    ret.setRect(0, 0, sliderstart, scrollBarRect.height());
                else
                    ret.setRect(0, 0, scrollBarRect.width(), sliderstart);
                break;
            case SC_ScrollBarAddPage:
                if (scrollbar->orientation == Qt::Horizontal)
                    ret.setRect(sliderstart + sliderlen, 0,
                                maxlen - sliderstart - sliderlen, scrollBarRect.height());
                else
                    ret.setRect(0, sliderstart + sliderlen, scrollBarRect.width(),
                                maxlen - sliderstart - sliderlen);
                break;
            case SC_ScrollBarSlider:
                 if (scrollbar->orientation == Qt::Horizontal)
                     ret.setRect(sliderstart, 0, sliderlen, scrollBarRect.height());
                 else
                     ret.setRect(0, sliderstart, scrollBarRect.width(), sliderlen);
                 break;
            default:
                break;
            }
        }
        return ret;
    }
    else
    {
        return QProxyStyle::subControlRect( control, option, subControl, widget );
    }
}

void StyleEbook::drawControl ( ControlElement element, const QStyleOption* option, QPainter* painter, const QWidget* widget ) const
{
    switch ( element )
    {
    case QStyle::CE_ScrollBarAddLine:
    case QStyle::CE_ScrollBarSubLine:
    case QStyle::CE_ScrollBarFirst:
    case QStyle::CE_ScrollBarLast:
        break;
    case QStyle::CE_ScrollBarAddPage:
    case QStyle::CE_ScrollBarSubPage:
        {
            QPalette pal(qApp->palette());
            painter->setPen( Qt::NoPen );
            painter->setBrush(pal.brush(QPalette::Window));
            painter->drawRect( option->rect );
        }
        break;
    case QStyle::CE_ScrollBarSlider:
        {
            painter->setPen( Qt::NoPen );
            painter->setBrush(QBrush(Qt::gray));
            painter->drawRect( option->rect );
        }
        break;
    default:
        QProxyStyle::drawControl(element, option, painter, widget);
        break;
    }
}

QStyle::SubControl StyleEbook::hitTestComplexControl(QStyle::ComplexControl control, const QStyleOptionComplex *option, const QPoint &position, const QWidget *widget) const
{
    return QStyle::CC_ScrollBar == control?
               SC_None :
               QProxyStyle::hitTestComplexControl(control, option, position, widget );
}
