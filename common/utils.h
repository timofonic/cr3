#ifndef H__UTILS
#define H__UTILS

#include <string>
#include <vector>

#define PSEP '/'
#define PSEPS "/"
#define SIZE(arr) (sizeof(arr)/sizeof(arr[0]))
#define STR_EXPAND(tok) #tok
#define STR(tok) STR_EXPAND(tok)

bool is_absolute_path( const char* path );
std::string get_ext(const std::string& str);
std::string get_base(const std::string& str);
std::string get_file(const std::string& str);
std::string get_path(const std::string& str);

bool is_file(const char* path);
bool is_dir(const char* path);
bool mk_dir(const char* dir);
bool mkpath(const char* path, bool skipLast /* = false */);

bool make_dir(const std::string& path);
bool make_path(const std::string& path);

void create_empty_file( const char* file );
void remove_file( const std::string& path );

int str2int(const char* str, int def=0);
std::string int2str( int val );

void splitInVector  (const std::string & text,
                     const std::string & separators,
                     std::vector<std::string> & words);

std::string vacuum( const std::string& val );
std::string to_upper( const std::string& val );

void str_to_file( const std::string& fileName, const std::string& str );
std::string str_from_file( const std::string& fileName );

#endif

