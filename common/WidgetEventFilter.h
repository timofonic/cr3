#ifndef WIDGETEVENTFILTER_H
#define WIDGETEVENTFILTER_H

#include <QObject>

class VirtualKeyboardContainer;

class WidgetEventFilter
{
    QWidget* tlw;
    VirtualKeyboardContainer* vkc;
public:
    WidgetEventFilter(QWidget* t, VirtualKeyboardContainer* v);
    virtual ~WidgetEventFilter() {}
    virtual bool filter(QObject *obj, QEvent *event);
};

#endif // WIDGETEVENTFILTER_H
