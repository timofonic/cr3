#include <QDir>
#include <QScrollBar>

#include "Platform.h"

#include "DictionaryWidget.h"

DictionaryWidget::DictionaryWidget(QWidget *parent)
    : QPlainTextEdit(parent)
    , gc( viewport() )
{
    QObject::connect( &sdcv, SIGNAL(readyReadStandardOutput()), this, SLOT(onProcessReadyReadStdOutput()) );
    QObject::connect( &sdcv, SIGNAL(readyReadStandardError()), this, SLOT(onProcessReadyReadStdError()) );
    QObject::connect( &sdcv, SIGNAL(finished(int)), this, SLOT(onProcessFinished(int)) );
    QObject::connect( &gc, SIGNAL(sigGesture(QPoint,GesturesController::GestureType)), this, SLOT(onGesture(QPoint,GesturesController::GestureType)) );
    setReadOnly(true);
}

void DictionaryWidget::translate(const QString& phrase)
{
    clear();
    if ( phrase.isEmpty() ) return;

    // get pure word without punctuation
    int st=0;
    for ( ; st<phrase.length() && !phrase[st].isLetter(); ++st )  {}
    int fi=phrase.length()-1;
    for ( ; fi>=0 && !phrase[fi].isLetter(); --fi )  {}
    if ( st > fi ) return;
    QString word( phrase.mid( st, fi-st+1 ) );
    if ( word.isEmpty() ) return;

    QString dictPath(qgetenv("VLASOVSOFT_DICT"));
    if ( dictPath.isEmpty() )
        dictPath = Platform::get()->getRootPath() + QDir::separator() + "dictionary";
    QString sdcvPath(dictPath + QDir::separator() + "sdcv");
    QStringList args;
    args.append("--data-dir");
    args.append(dictPath);
    args.append("--non-interactive");
    args.append("--utf8-input");
    args.append("--utf8-output");
    args.append(word);
    sdcv.start(sdcvPath, args);
}

void DictionaryWidget::onProcessReadyReadStdOutput()
{  
    QString text(QString::fromUtf8(sdcv.readAllStandardOutput().constData()));
    text.replace("&apos;", "'");
    appendPlainText(text);
}

void DictionaryWidget::onProcessReadyReadStdError()
{  
    QString text(QString::fromUtf8(sdcv.readAllStandardError().constData()));
    appendPlainText(text);
}

void DictionaryWidget::onProcessFinished(int)
{
    verticalScrollBar()->setValue(verticalScrollBar()->minimum());
    emit signalTranslateFinish();
}

void DictionaryWidget::onGesture(QPoint, GesturesController::GestureType t)
{
    int val = 0;
    QScrollBar* pSb = verticalScrollBar();
    switch ( t )
    {
    case GesturesController::gtSwipeBT:
        val = pSb->value() + pSb->pageStep();
        if ( val > pSb->maximum() ) val = pSb->maximum();
        pSb->setValue( val);
        break;
    case GesturesController::gtSwipeTB:
        val = pSb->value() - pSb->pageStep();
        if ( val < pSb->minimum() ) val = pSb->minimum();
        pSb->setValue( val);
        break;
    case GesturesController::gtSwipeRL:
        pSb->setValue( pSb->maximum() );
        break;
    case GesturesController::gtSwipeLR:
        pSb->setValue( pSb->minimum() );
        break;
    default:
        break;
    }
}
