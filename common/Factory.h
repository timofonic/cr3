#ifndef FACTORY_H
#define FACTORY_H

#include <QVector>
#include <QFile>
#include <QDir>
#include <QTextStream>
#include <QDateTime>

#include "Platform.h"
#include "Exception.h"

template <typename T>
class Factory: public QVector<T>
{
    static Factory<T>* pFactory;

    Factory() {}

public:
    static Factory<T>* instance()
    {
        if ( !pFactory )
        {
            pFactory = new Factory<T>();
        }
        return pFactory;
    }
    static void destroy()
    {
        if ( pFactory )
        {
            delete pFactory;
            pFactory = NULL;
        }
    }
    void init( const QString& fn )
    {
        QFileInfo fi(fn);
        QFile file( fi.isAbsolute() ? fn : Platform::get()->getRootPath() + QDir::separator() + fn );
        if ( file.open( QIODevice::ReadOnly | QIODevice::Text ) )
        {
            QTextStream in(&file);
            in.setCodec("UTF-8");
            while(!in.atEnd())
            {
                QString line = in.readLine();
                this->push_back( T(line) );
            }
        }

        if ( this->empty() )
            throw Exception(true, "Error reading file: %s", file.encodeName(fi.fileName()).constData());
    }
};

#endif // FACTORY_H
