#include <QDir>
#include <QStringList>

#include "Platform.h"
#include "Exception.h"
#include "Config.h"
#include "Factory.h"

#include "LanguageDescriptor.h"

template <>
Factory<LanguageDescriptor>* Factory<LanguageDescriptor>::pFactory = NULL;

LanguageDescriptor::LanguageDescriptor()
{}

LanguageDescriptor::LanguageDescriptor( const QString& str )
{
    static const char* msg = "Invalid language description: %s [File: %s, Line: %d]";

    QStringList list(str.split("|"));

    if ( list.size() < 5 )
    {
        throw Exception( true, msg, str.toUtf8().constData(), __FILE__, __LINE__ );
    }

    name   = list.at(0).trimmed();
    native = list.at(1).trimmed();
    trans1 = list.at(2).trimmed();
    trans2 = list.at(3).trimmed();
    kbd    = list.at(4).trimmed();
}

QString LanguageDescriptor::getTrans1FileName() const
{
    return Platform::get()->getLocalTranslationsPath() + QDir::separator() + trans1;
}

QString LanguageDescriptor::getTrans2FileName() const
{
    return Platform::get()->getCommonTranslationsPath() + QDir::separator() + trans2;
}

QString LanguageDescriptor::getKbdFileName() const
{
    return Platform::get()->getCommonTranslationsPath() + QDir::separator() + kbd;
}
