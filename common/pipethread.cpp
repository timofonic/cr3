#include <QFile>
#include <QTextStream>
#include <QDebug>

#include "Log.h"
#include "QtUtils.h"

#include "pipethread.h"

PipeThread::PipeThread( const QString& p ) 
    : QThread(NULL)
    , pipe(p)
{
    start();
}

PipeThread::~PipeThread()
{
    if ( isRunning() ) 
    {
        writeFifoCommand(pipe, "quit");
        wait();
    }
}

void PipeThread::run()
{
    QFile f(pipe);
    g_pLog->write(1, Log::MT_I, "Pipe thread started: %s", QFile::encodeName(pipe).constData());
    while ( true )
    {
        if (f.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            QTextStream in(&f);
            QString s = in.readLine();
            f.close();
            if ( s == "quit" ) // used to stop pipe thread when app exitting
                break;
            else
            {
                emit sigPipeMsg(s);
                g_pLog->write(2, Log::MT_I, "Pipe thread command: %s", s.toLatin1().constData());
            }
        }
        else
            break;
    }
    g_pLog->write(1, Log::MT_I, "Pipe thread finished");
}

