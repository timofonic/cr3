#ifndef QTUTILS_H_
#define QTUTILS_H_

#include <QMessageBox>
#include <QString>
#include <QDateTime>

class QString;
class QWidget;
class QAbstractScrollArea;
class QTextBrowser;

QString v1();

void centerWidget( QWidget* w, const QRect& r );
void centerWidget( QWidget* w );

void messageBox( QWidget* parent,
                 QMessageBox::Icon icon,
                 const QString& title,
                 const QString& text );

void messageBox( QWidget* parent,
                 QMessageBox::Icon icon,
                 const char* title,
                 const char* text );

QMessageBox::StandardButton
questionBox( QWidget* parent,
             const QString& title,
             const QString& text,
             QMessageBox::StandardButtons buttons = QMessageBox::Ok,
             QMessageBox::StandardButton defaultButton = QMessageBox::NoButton );

QString getOpenFileName(QWidget* parent, const QString& caption, const QString& path, const QString& filter);
QString getSaveFileName(QWidget* parent, const QString& caption, const QString& path, const QString& filter, const QString& defSuffix = QString());

QString get_time_as_string(int ms);

qint64 msecsTo(const QDateTime& t1, const QDateTime& t2);

void touch_file( const QString& fileName );
QString str_from_file( const QString& fileName );
int int_from_file( const QString& fileName );
#if !defined(IOS)
QString str_from_proc( const QString& cmd );
#endif
void str_to_file( const QString& fileName, const QString& str );

void initTopLevelWidget( QWidget* w );

void writeFifoCommand(const QString& fifo, const char* cmd);

QSize getSizePx( const QSize& sentiInches, int screen );

QString get_book_cover_path();

bool is_eink();

int in_to_px( qreal inches ); // inches to pixels
int mm_to_px( qreal mm );     // millimeters to pixels

#if !defined(DESKTOP)
void activate_scroller( QAbstractScrollArea* pArea );
#endif

void resize_font( QWidget* w, int nom, int denom );

QString to_rgba( const QColor& color );

void readTextBrowser( QTextBrowser* textBrawser, const QString& file );

#endif /* QTUTILS_H_ */
