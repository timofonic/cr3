QT += svg network
INCLUDEPATH += . ../common ../common/platform

kindle {
    ARCH = kindle
    CONFIG += eink release
    DEFINES += QT_KEYPAD_NAVIGATION KINDLE LINUX
    HEADERS += QWSPlatform.h KindlePlatform.h
    lib {
        SOURCES += QWSPlatform.cpp KindlePlatform.cpp
    }
}

obreey {
    ARCH = obreey
    DESTDIR = obj_qt_$$ARCH
    CONFIG += eink release
}

pb {
    ARCH = pb
    DESTDIR = obj_qt_$$ARCH 
    CONFIG += eink release
}

kobo {
    ARCH = kobo
    CONFIG -= debug
    CONFIG += eink release
    DEFINES += QT_KEYPAD_NAVIGATION KOBO LINUX
    HEADERS += QWSPlatform.h KoboPlatform.h
    lib {
        SOURCES += QWSPlatform.cpp KoboPlatform.cpp
    }
    LIBS += -ldl
}

linux-g++|linux-g++-64 {
    greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
    DEFINES += LINUX
    simple {
        DEFINES += SIMPLE
        CONFIG += simple
        ARCH += simple
        DESTDIR = obj_qt_simple
    }
    else {
        DEFINES += DESKTOP
        CONFIG += desktop
        ARCH = x86_64
        DESTDIR = obj_qt_x86_64
    }
    HEADERS += DesktopPlatform.h
    lib {
        SOURCES += DesktopPlatform.cpp
    }
}

linux-g++-32 {
    greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
    ARCH = x86_32
    CONFIG += desktop
    DEFINES += DESKTOP LINUX
    HEADERS += DesktopPlatform.h
    lib {
        SOURCES += DesktopPlatform.cpp
    }
}

windows {
    ARCH = windows
    CONFIG += desktop exceptions
    DEFINES += DESKTOP WINDOWS
    HEADERS += DesktopPlatform.h
    lib {
        SOURCES += DesktopPlatform.cpp
    }
}

macx {
    QMAKE_MAC_SDK = macosx10.11
    Qt += widgets
    ARCH = osx
    DESTDIR = obj_qt_osx
    CONFIG += desktop exceptions
    DEFINES += DESKTOP MACX
    HEADERS += DesktopPlatform.h
    QMAKE_CFLAGS += -gdwarf-2
    QMAKE_CXXFLAGS += -gdwarf-2
    lib {
        SOURCES += DesktopPlatform.cpp
    }
} 

ios {
    Qt += widgets
    CONFIG += release
    ARCH = ios
    DESTDIR = obj_qt_ios
    DEFINES += IOS
    HEADERS += IOSPlatform.h
    lib {
        SOURCES += IOSPlatform.cpp
        RESOURCES += resources/eink/eink.qrc
    }
}

qvfb {
    ARCH = qvfb
    CONFIG += debug
    CONFIG -= release
    DESTDIR = obj_qt_qvfb
    DEFINES += QVFB QT_KEYPAD_NAVIGATION LINUX EINK
    HEADERS += QWSPlatform.h
    lib {
        SOURCES += QWSPlatform.cpp
    }
}

android_armv5 {
    ARCH = android_armv5
}

android_armv7 {
    ARCH = android_armv7
}

android_x86 {
    ARCH = android_x86
}

pb|obreey {
    DEFINES += INKVIEW INKVIEW_PRO QT_KEYPAD_NAVIGATION LINUX
    INCLUDEPATH += ../QtIvPlugins/IvInput
    HEADERS += QWSPlatform.h InkViewPlatform.h
    pb {
        LIBS += -L/usr/arm-none-linux-gnueabi/lib
    }
    obreey {
        DEFINES += OBREEY
    }
    lib {
        SOURCES += QWSPlatform.cpp InkViewPlatform.cpp
    }
    else {
        LIBS += -L../QtIvPlugins/IvInput/$$DESTDIR -lIvInput -linkview
    }
}

android_armv5|android_armv7|android_x86 {
    QT      += network widgets
    CONFIG  += android
    HEADERS += AndroidPlatform.h
    DEFINES += ANDROID
    lib {
        SOURCES += AndroidPlatform.cpp
        RESOURCES += resources/eink/eink.qrc
    }
}

eink {
    DEFINES += EINK
}

lib {
    RESOURCES += resources/keyboard/keyboard.qrc resources/rotation/rotation.qrc
    eink|qvfb|simple {
        RESOURCES += resources/eink/eink.qrc
    }
    INCLUDEPATH += platform
    VPATH       += platform
}
else {
    LIBS += -L../common/$$DESTDIR -lcommon
    !debug {
        QMAKE_LFLAGS += -Wl,-s
    }
    VPATH       += ../common ../common/platform
    INCLUDEPATH += ../common ../common/platform
}

OBJECTS_DIR = $$DESTDIR/obj
MOC_DIR = $$DESTDIR/moc
RCC_DIR = $$DESTDIR/rcc
UI_DIR = $$DESTDIR/ui
INCLUDEPATH += ../common/$$UI_DIR
