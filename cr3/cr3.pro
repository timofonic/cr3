TARGET = cr3
VERSION = 0.1.4
TEMPLATE = app

include(../common/common.pri)
include(../common/i18n.pri)

DEFINES += USE_FONTCONFIG=0 USE_FREETYPE=1 LDOM_USE_OWN_MEM_MAN=1 COLOR_BACKBUFFER=1 USE_DOM_UTF8_STORAGE=1 NDEBUG=1 QT_KEYPAD_NAVIGATION

INCLUDEPATH += $$(QTDIR)/include/freetype2 include \
    ../../crengine/thirdparty/antiword

SOURCES += main.cpp \
    mainwindow.cpp \
    searchdlg.cpp \
    filepropsdlg.cpp \
    cr3widget.cpp \
    crqtutil.cpp \
    tocdlg.cpp \
    recentdlg.cpp \
    settings.cpp \
    bookmarklistdlg.cpp \
    openfiledlg.cpp \
    orientation.cpp \
    actionlist.cpp \
    mainmenu.cpp \
    gotodialog.cpp \
    progbareventfilter.cpp \
    navigationbar.cpp \
    keymapper.cpp \
    actiondisplayer.cpp \
    recentremovedlg.cpp \
    dictionary.cpp \
    application.cpp

HEADERS += mainwindow.h \
    cr3widget.h \
    crqtutil.h \
    tocdlg.h \
    recentdlg.h \
    settings.h \
    bookmarklistdlg.h \
    searchdlg.h \
    filepropsdlg.h \
    openfiledlg.h \
    orientation.h \
    actionlist.h \
    mainmenu.h \
    gotodialog.h \
    progbareventfilter.h \
    navigationbar.h \
    props.h \
    keymapper.h \
    actiondisplayer.h \
    recentremovedlg.h \
    dictionary.h \
    application.h

FORMS += mainwindow.ui \
    tocdlg.ui \
    recentdlg.ui \
    settings.ui \
    bookmarklistdlg.ui \
    searchdlg.ui \
    filepropsdlg.ui \
    openfiledlg.ui \
    gotodialog.ui \
    navigationbar.ui \
    recentremovedlg.ui \
    dictionary.ui

RESOURCES += cr3.qrc

LIBS += -lQtGui -lQtCore -lQtNetwork -lpthread -ldl

LIBS += -Lqtbuild_$$ARCH/crengine \
        -Lqtbuild_$$ARCH/thirdparty/antiword \
        -Lqtbuild_$$ARCH/thirdparty/chmlib

kobo|kindle|pb {
LIBS += -Lqtbuild_$$ARCH/thirdparty/zlib \ 
        -Lqtbuild_$$ARCH/thirdparty/libpng \
        -Lqtbuild_$$ARCH/thirdparty/libjpeg \
        -Lqtbuild_$$ARCH/thirdparty/freetype
}

LIBS += -lcrengine -lz -lantiword -lchmlib -lpng -ljpeg -lfreetype -lcommon

