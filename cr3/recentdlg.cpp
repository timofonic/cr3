#include <QDir>
#include <QItemDelegate>
#include <QPainter>
#include <QFile>

#include "QtUtils.h"

#include "props.h"
#include "openfiledlg.h"
#include "recentremovedlg.h"

#include "recentdlg.h"
#include "ui_recentdlg.h"

class RecentTableWidgetItem: public QTableWidgetItem
{
public:
    QString title;
    QString info;
    QString percent;
};

class RecentBooksListDelegate : public QItemDelegate
{
    mutable QFont fontTitle;
    mutable QFont fontInfo;

public:
    static const int GAP  = 5; // gap above and below text in the cell
    static const int LINE = 5; // selected item line width

    RecentBooksListDelegate(QObject* pobj)
        : QItemDelegate(pobj)
    {
    }

    void paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const
    {
        QStyleOptionViewItem newOption;
        newOption = option;

        // paint selection background
        QItemDelegate::paint(painter, option, index);

        // set text color
        QPen textPen( newOption.palette.color( newOption.state & QStyle::State_Selected ? QPalette::HighlightedText : QPalette::Text ));
        painter->setPen( textPen );

        fontTitle = option.font;
        fontTitle.setBold(true);
        fontInfo = option.font;
        fontInfo.setItalic(true);

        QTableWidget* pWidget = static_cast<QTableWidget*>( parent() );
        RecentTableWidgetItem* pItem = static_cast<RecentTableWidgetItem*>(pWidget->item(index.row(), index.column()));

        painter->setFont(fontTitle);
        QFontMetrics tfm(fontTitle);
        int fontTitleSize = tfm.height();
        QRect titleRect( newOption.rect.x(), newOption.rect.y(), newOption.rect.width(), fontTitleSize );
        titleRect.translate( 0, GAP );
        painter->drawText( titleRect, Qt::TextSingleLine, tfm.elidedText(pItem->title, Qt::ElideRight, newOption.rect.width() ));

        painter->setFont(fontInfo);
        QFontMetrics ifm(fontInfo);
        int fontAuthorSize = ifm.height();
        QRect infoRect( newOption.rect.x(), titleRect.bottom(), newOption.rect.width(), fontAuthorSize );

        int percentWidth = ifm.boundingRect(pItem->percent).width();
        infoRect.adjust(0,0,-percentWidth,0);

        painter->drawText( infoRect, Qt::TextSingleLine, ifm.elidedText(pItem->info, Qt::ElideRight, infoRect.width() ) );

        QRect percentRect( infoRect.right(), infoRect.top(), percentWidth, infoRect.height() );
        painter->drawText( percentRect, Qt::TextSingleLine, pItem->percent );
    }
};

RecentBooksDlg::RecentBooksDlg(QWidget *parent, CR3View * docView )
    : Dialog(parent)
    , ui(new Ui::RecentBooksDlg)
    , docview(docView)
{
    ui->setupUi(this);
    init();

    QFont f(ui->lblPosition->font());
    f.setPointSize(f.pointSize()/2);
    ui->lblPosition->setFont(f);

    f = ui->lblTitle->font();
    f.setBold(true);
    ui->lblTitle->setFont(f);

    ui->tableWidget->setColumnCount(1);

    ui->tableWidget->horizontalHeader()->setResizeMode(0, QHeaderView::Stretch);
    ui->tableWidget->horizontalHeader()->setResizeMode(1, QHeaderView::ResizeToContents);
    ui->tableWidget->verticalHeader()->setResizeMode(QHeaderView::Custom);

    // fill rows
    ui->tableWidget->verticalHeader()->setResizeMode(QHeaderView::Fixed);
    ui->tableWidget->setItemDelegate(new RecentBooksListDelegate(ui->tableWidget));

    LVPtrVector<CRFileHistRecord>& files = docview->getDocView()->getHistory()->getRecords();

    // remove non-existing entries
    for( int k=0; k<files.length(); )
    {
        QString fn(cr2qt(files.get(k)->getFilePathName()));
        if (!QFile::exists(fn))
            files.remove(k);
        else
            ++k;
    }

    ui->tableWidget->setRowCount( files.length() );

    // insert items
    for( int index=0; index<files.length(); ++index )
    {
        CRFileHistRecord* book = files.get(index);

        RecentTableWidgetItem* titleItem = new RecentTableWidgetItem();
        titleItem->title   = cr2qt(book->getTitle());
        if ( titleItem->title.isEmpty() )
            titleItem->title = cr2qt(book->getFileName());
        titleItem->info  = cr2qt(book->getAuthor());
        if ( titleItem->info.isEmpty() )
            titleItem->info = "-";
        titleItem->percent = " " + crpercent(book->getLastPos()->getPercent());

        ui->tableWidget->setItem(index, 0, titleItem);
    }

    ui->navBar->setControls(ui->tableWidget, ui->lblPosition,
                            QSize( docview->getOptions()->getIntDef(PROP_APP_UI_SWIPES_X_WEIGHT, 1),
                                   docview->getOptions()->getIntDef(PROP_APP_UI_SWIPES_Y_WEIGHT, 1) ) );

    ui->tableWidget->setEditFocus(true);
    ui->tableWidget->setCurrentCell(0,0);

    QObject::connect( ui->tableWidget, SIGNAL(itemActivated(QTableWidgetItem*)), this, SLOT(onClicked(QTableWidgetItem*)) );
    // single click is just selection
    // QObject::connect( ui->tableWidget, SIGNAL(itemClicked(QTableWidgetItem*)), this, SLOT(onClicked(QTableWidgetItem*)) );

    QTimer::singleShot(0, this, SLOT(deferredInit()));
}

RecentBooksDlg::~RecentBooksDlg()
{
    delete ui;
}

int RecentBooksDlg::getBookNum()
{
    return ui->tableWidget->currentRow();
}

void RecentBooksDlg::keyPressEvent(QKeyEvent *e)
{
    switch( e->key() )
    {
    case Qt::Key_Menu:
        if ( ui->tableWidget->hasFocus() )
            ui->btnFolder->setFocus();
        else
            ui->tableWidget->setEditFocus(true);
        break;
    default:
        break;
    }
}

void RecentBooksDlg::accept()
{
    int bn = getBookNum();
    if ( bn >= 0 )
    {
        LVPtrVector<CRFileHistRecord>& files = docview->getDocView()->getHistory()->getRecords();
        fileName = cr2qt(files[bn]->getFilePathName());
        QDialog::accept();
    }
}

void RecentBooksDlg::on_btnOk_clicked()
{
    accept();
}

void RecentBooksDlg::on_btnFolder_clicked()
{
    int bn = getBookNum();
    if ( bn >= 0 )
    {
        LVPtrVector<CRFileHistRecord>& files = docview->getDocView()->getHistory()->getRecords();
        QString path(cr2qt(files[bn]->getFilePath()));
        OpenFileDlg dlg( this, docview, path );
        if ( dlg.exec() && !dlg.fileName.isEmpty() )
        {
            fileName = dlg.fileName;
            QDialog::accept();
        }
    }
}

void RecentBooksDlg::on_btnRemove_clicked()
{
    int bn = getBookNum();
    if ( bn >= 0 )
    {
        LVPtrVector<CRFileHistRecord>&
                files = docview->getDocView()->getHistory()->getRecords();

        int result = 0;
        RecentRemoveDlg dlg(this);
        dlg.setFullScreen(false);
        dlg.exec();
        result = dlg.getResult();

        switch( result )
        {
        case 0:
        default:
            break;
        case 1:
            if ( bn > 0 || !docview->getDocView()->isDocumentOpened() )
            {
                files.remove(bn);
                ui->tableWidget->removeRow(bn);
            }
            break;
        case 2:
            if ( 0 == bn && docview->getDocView()->isDocumentOpened() )
            {
                docview->getDocView()->createDefaultDocument( qt2cr(tr("Removed")), qt2cr(tr("Document was removed") ) );
            }
            {
                QString fileName(cr2qt(files[bn]->getFilePathName()));
                files.remove(bn);
                ui->tableWidget->removeRow(bn);
                QFile::remove(fileName);
                docview->update();
            }
            break;
        }
    }
}

void RecentBooksDlg::onClicked(QTableWidgetItem*)
{
    accept();
}

void RecentBooksDlg::deferredInit()
{
    // fill rows
    QFont f(ui->tableWidget->font());
    QFontMetrics fm(f);
    int rowHeight = 2*fm.height() + 2*RecentBooksListDelegate::GAP;
    ui->tableWidget->verticalHeader()->setDefaultSectionSize(rowHeight);
}
