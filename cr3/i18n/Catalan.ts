<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ca" sourcelanguage="en_US">
<context>
    <name>BookmarkListDialog</name>
    <message>
        <source>Bookmarks</source>
        <translation>Marcadors</translation>
    </message>
    <message>
        <source>Position</source>
        <translation>Posició</translation>
    </message>
    <message>
        <source>Text</source>
        <translation>Text</translation>
    </message>
</context>
<context>
    <name>CR3View</name>
    <message>
        <source>Error while opening document </source>
        <translation>Error obrint el document </translation>
    </message>
    <message>
        <source>Loading: please wait...</source>
        <translation>espera...</translation>
    </message>
</context>
<context>
    <name>FilePropsDialog</name>
    <message>
        <source>Document properties</source>
        <translation>propietats</translation>
    </message>
    <message>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <source>Archive name</source>
        <translation>Nom de l’arxiu</translation>
    </message>
    <message>
        <source>Archive path</source>
        <translation>Ruta de l’arxiu</translation>
    </message>
    <message>
        <source>Archive size</source>
        <translation>Mida de l’arxiu</translation>
    </message>
    <message>
        <source>File name</source>
        <translation>Nom de l’arxiu</translation>
    </message>
    <message>
        <source>File path</source>
        <translation>Ruta de l’arxiu</translation>
    </message>
    <message>
        <source>File size</source>
        <translation>Mida de l’arxiu</translation>
    </message>
    <message>
        <source>File format</source>
        <translation>Format de l’arxiu</translation>
    </message>
    <message>
        <source>File info</source>
        <translation>Informació de l’arxiu</translation>
    </message>
    <message>
        <source>Title</source>
        <translation>Títol</translation>
    </message>
    <message>
        <source>Author(s)</source>
        <translation>Autor/s</translation>
    </message>
    <message>
        <source>Series name</source>
        <translation>Nom de la sèrie</translation>
    </message>
    <message>
        <source>Series number</source>
        <translation>Número de la sèrie</translation>
    </message>
    <message>
        <source>Date</source>
        <translation>Data</translation>
    </message>
    <message>
        <source>Genres</source>
        <translation>Gèneres</translation>
    </message>
    <message>
        <source>Translator</source>
        <translation>Traductor</translation>
    </message>
    <message>
        <source>Book info</source>
        <translation>Informació del llibre</translation>
    </message>
    <message>
        <source>Document author</source>
        <translation>Autor del document</translation>
    </message>
    <message>
        <source>Document date</source>
        <translation>Data del document</translation>
    </message>
    <message>
        <source>Document source URL</source>
        <translation>URL de la font del document</translation>
    </message>
    <message>
        <source>OCR by</source>
        <translation>OCR per</translation>
    </message>
    <message>
        <source>Document version</source>
        <translation>Versió del document</translation>
    </message>
    <message>
        <source>Document info</source>
        <translation>Informació del document</translation>
    </message>
    <message>
        <source>Publication name</source>
        <translation>Nom de la publicació</translation>
    </message>
    <message>
        <source>Publisher</source>
        <translation>Editor</translation>
    </message>
    <message>
        <source>Publisher city</source>
        <translation>Ciutat de l’editor</translation>
    </message>
    <message>
        <source>Publication year</source>
        <translation>Any de l’edició</translation>
    </message>
    <message>
        <source>ISBN</source>
        <translation>ISBN</translation>
    </message>
    <message>
        <source>Publication info</source>
        <translation>Informació de la publicació</translation>
    </message>
    <message>
        <source>Custom info</source>
        <translation>Informació personalitzada</translation>
    </message>
</context>
<context>
    <name>GoToDialog</name>
    <message>
        <source>Position</source>
        <translation>Posició</translation>
    </message>
    <message>
        <source>Page</source>
        <translation>Pàgina</translation>
    </message>
    <message>
        <source>Go</source>
        <translation>Anar</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Cancel·lar</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>Navigation</source>
        <translation>Navegació</translation>
    </message>
    <message>
        <source>Not found</source>
        <translation>No s’ha trobat</translation>
    </message>
    <message>
        <source>Search pattern is not found in document</source>
        <translation>Patró de cerca no trobat</translation>
    </message>
    <message>
        <source>Bookmark created</source>
        <translation>S&apos;ha creat el marcador</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="unfinished">Esborrar</translation>
    </message>
    <message>
        <source>Do you really want to delete the current document?</source>
        <translation>Realment vols eliminar aquest document?</translation>
    </message>
    <message>
        <source>Removed</source>
        <translation>Eliminat</translation>
    </message>
    <message>
        <source>Document was removed</source>
        <translation>Document eliminat</translation>
    </message>
</context>
<context>
    <name>MainWindowClass</name>
    <message>
        <source>Open file</source>
        <translation>Obrir arxiu</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>Tancar</translation>
    </message>
    <message>
        <source>Next page</source>
        <translation>Avançar pàgina</translation>
    </message>
    <message>
        <source>Previous page</source>
        <translation>Retrocedir pàgina</translation>
    </message>
    <message>
        <source>Back</source>
        <translation>Enrera</translation>
    </message>
    <message>
        <source>Forward</source>
        <translation>Endavant</translation>
    </message>
    <message>
        <source>Table of contents</source>
        <translation>Taula de continguts</translation>
    </message>
    <message>
        <source>Recent books</source>
        <translation>Darrers llibres oberts</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Configuració</translation>
    </message>
    <message>
        <source>Add bookmark</source>
        <translation>Afegir marcador</translation>
    </message>
    <message>
        <source>Bookmarks</source>
        <translation>Marcadors</translation>
    </message>
    <message>
        <source>File properties</source>
        <translation>Propietats de l’arxiu</translation>
    </message>
    <message>
        <source>Find text</source>
        <translation>Buscar text</translation>
    </message>
    <message>
        <source>Show menu</source>
        <translation>Mostrar menú</translation>
    </message>
    <message>
        <source>Empty</source>
        <translation>Cap acció</translation>
    </message>
    <message>
        <source>Front light</source>
        <translation>Llum integrada</translation>
    </message>
    <message>
        <source>Screen rotation</source>
        <translation>Rotació de la pantalla</translation>
    </message>
    <message>
        <source>Toggle frontlight</source>
        <translation>Encendre/apagar llum</translation>
    </message>
    <message>
        <source>Frontlight &gt;</source>
        <translation>Llum integrada &gt;</translation>
    </message>
    <message>
        <source>Frontlight &lt;</source>
        <translation>Llum integrada &lt;</translation>
    </message>
    <message>
        <source>First page</source>
        <translation>Primera pàgina</translation>
    </message>
    <message>
        <source>Last page</source>
        <translation>Darrera pàgina</translation>
    </message>
    <message>
        <source>Next chapter</source>
        <translation>Capítol següent</translation>
    </message>
    <message>
        <source>Previous chapter</source>
        <translation>Capítol anterior</translation>
    </message>
    <message>
        <source>Next 10 pages</source>
        <translation>Avançar 10 pàgines</translation>
    </message>
    <message>
        <source>Previous 10 pages</source>
        <translation>Retrocedir 10 pàgines</translation>
    </message>
    <message>
        <source>Zoom In</source>
        <translation>Ampliar</translation>
    </message>
    <message>
        <source>Zoom Out</source>
        <translation>Reduir</translation>
    </message>
    <message>
        <source>Position</source>
        <translation>Posició</translation>
    </message>
    <message>
        <source>Toggle inversion</source>
        <translation>Normal/invertit</translation>
    </message>
    <message>
        <source>Toggle header</source>
        <translation>Capçalera si/no</translation>
    </message>
    <message>
        <source>Suspend</source>
        <translation>Suspendre</translation>
    </message>
    <message>
        <source>Screen rotation 0</source>
        <translation>Rotació de la pantalla 0</translation>
    </message>
    <message>
        <source>Screen rotation 90</source>
        <translation>Rotació de la pantalla 90</translation>
    </message>
    <message>
        <source>Screen rotation 180</source>
        <translation>Rotació de la pantalla 180</translation>
    </message>
    <message>
        <source>Screen rotation 270</source>
        <translation>Rotació de la pantalla 270</translation>
    </message>
    <message>
        <source>Screen rotation +90</source>
        <translation>Rotació de la pantalla +90</translation>
    </message>
    <message>
        <source>Screen rotation -90</source>
        <translation>Rotació de la pantalla -90</translation>
    </message>
    <message>
        <source>Screen rotation +180</source>
        <translation>Rotació de la pantalla +180</translation>
    </message>
    <message>
        <source>Delete current document</source>
        <translation>Eliminar document actual</translation>
    </message>
    <message>
        <source>Dictionary</source>
        <translation>Diccionari</translation>
    </message>
</context>
<context>
    <name>NavigationBar</name>
    <message>
        <source>%1-%2 (total %3)</source>
        <translation>%1-%2 (total %3)</translation>
    </message>
</context>
<context>
    <name>OpenFileDlg</name>
    <message>
        <source>Open file</source>
        <translation>Obrir arxiu</translation>
    </message>
</context>
<context>
    <name>RecentBooksDlg</name>
    <message>
        <source>Recent books</source>
        <translation>Darrers llibres</translation>
    </message>
    <message>
        <source>Removed</source>
        <translation>Eliminat</translation>
    </message>
    <message>
        <source>Document was removed</source>
        <translation>Document eliminat</translation>
    </message>
</context>
<context>
    <name>RecentRemoveDlg</name>
    <message>
        <source>Delete</source>
        <translation type="unfinished">Esborrar</translation>
    </message>
    <message>
        <source>Delete record only</source>
        <translation>Només esborrar registre</translation>
    </message>
    <message>
        <source>Delete record and file</source>
        <translation>Esborrar registre i arxiu</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished">Cancel·lar</translation>
    </message>
</context>
<context>
    <name>SearchDialog</name>
    <message>
        <source>Search</source>
        <translation>Buscar</translation>
    </message>
    <message>
        <source>Text</source>
        <translation>Text</translation>
    </message>
    <message>
        <source>Search forward</source>
        <translation>Cerca endavant</translation>
    </message>
    <message>
        <source>Search backward</source>
        <translation>Cerca enrera</translation>
    </message>
    <message>
        <source>Case sensitive</source>
        <translation>majúscules i minúscules</translation>
    </message>
</context>
<context>
    <name>SettingsDlg</name>
    <message>
        <source>Settings</source>
        <translation>Configuració</translation>
    </message>
    <message>
        <source>Window</source>
        <translation>Finestra</translation>
    </message>
    <message>
        <source>Startup</source>
        <translation>Iniciar</translation>
    </message>
    <message>
        <source>Recent book</source>
        <translation>Darrer llibre</translation>
    </message>
    <message>
        <source>Recent books list</source>
        <translation>Darrers llibres oberts</translation>
    </message>
    <message>
        <source>Open file</source>
        <translation>Obrir arxiu</translation>
    </message>
    <message>
        <source>Do nothing</source>
        <translation>Cap acció</translation>
    </message>
    <message>
        <source>Formatting</source>
        <translation>Format</translation>
    </message>
    <message>
        <source>Disable txt auto formatting</source>
        <translation>Deshabilitar l’autoformat de text</translation>
    </message>
    <message>
        <source>Internal styles</source>
        <translation>Estils interns</translation>
    </message>
    <message>
        <source>Embedded fonts</source>
        <translation>Fonts incrustades</translation>
    </message>
    <message>
        <source>Page</source>
        <translation>Pàgina</translation>
    </message>
    <message>
        <source>Header</source>
        <translation>Capçalera</translation>
    </message>
    <message>
        <source>Show</source>
        <translation>Mostrar</translation>
    </message>
    <message>
        <source>Font</source>
        <translation>Font</translation>
    </message>
    <message>
        <source>Face</source>
        <translation>Nom</translation>
    </message>
    <message>
        <source>Size</source>
        <translation>Mida</translation>
    </message>
    <message>
        <source>Elements</source>
        <translation>Elements</translation>
    </message>
    <message>
        <source>Chapter marks</source>
        <translation>Marcador de capítols</translation>
    </message>
    <message>
        <source>Book name</source>
        <translation>Nom del llibre</translation>
    </message>
    <message>
        <source>Clock</source>
        <translation>Rellotge</translation>
    </message>
    <message>
        <source>Position percent</source>
        <translation>Posició com a percentatge</translation>
    </message>
    <message>
        <source>Page number/count</source>
        <translation>Número de la pàgina</translation>
    </message>
    <message>
        <source>Battery status</source>
        <translation>Estat bateria</translation>
    </message>
    <message>
        <source>In percent</source>
        <translation>Percentatge</translation>
    </message>
    <message>
        <source>Footer</source>
        <translation>Peu de pàgina</translation>
    </message>
    <message>
        <source>Misc</source>
        <translation>miscel·lània</translation>
    </message>
    <message>
        <source>View Mode</source>
        <translation>Vista</translation>
    </message>
    <message>
        <source>One page</source>
        <translation>Una pàgina</translation>
    </message>
    <message>
        <source>Two pages</source>
        <translation>Dues pàgines</translation>
    </message>
    <message>
        <source>Update interval</source>
        <translation>Refrescar cada</translation>
    </message>
    <message>
        <source>Negative</source>
        <translation>Negatiu</translation>
    </message>
    <message>
        <source>Styles</source>
        <translation>Estils</translation>
    </message>
    <message>
        <source>Gamma</source>
        <translation>Gamma</translation>
    </message>
    <message>
        <source>Aa</source>
        <translation>Aa</translation>
    </message>
    <message>
        <source>Disabled</source>
        <translation>Deshabilitat</translation>
    </message>
    <message>
        <source>On for large fonts</source>
        <translation>Habilitat per a fonts grans</translation>
    </message>
    <message>
        <source>On for all fonts</source>
        <translation>Habilitat totes les fonts</translation>
    </message>
    <message>
        <source>Hinting</source>
        <translation>Al·lusionant</translation>
    </message>
    <message>
        <source>Bytecode</source>
        <translation>Codi de bit</translation>
    </message>
    <message>
        <source>Auto</source>
        <translation>Auto</translation>
    </message>
    <message>
        <source>Bold</source>
        <translation>Negreta</translation>
    </message>
    <message>
        <source>Kerning</source>
        <translation>Interlletratge</translation>
    </message>
    <message>
        <source>Layout</source>
        <translation>Disposició</translation>
    </message>
    <message>
        <source>Hyphenation</source>
        <translation>Separació sil·làbica</translation>
    </message>
    <message>
        <source>Spacing</source>
        <translation>Espaiament</translation>
    </message>
    <message>
        <source>Space</source>
        <translation>Espai</translation>
    </message>
    <message>
        <source>Floating punctuation</source>
        <translation>Puntuació flotant</translation>
    </message>
    <message>
        <source>Margins</source>
        <translation>Marges</translation>
    </message>
    <message>
        <source>Top</source>
        <translation>Superior</translation>
    </message>
    <message>
        <source>Bottom</source>
        <translation>Inferior</translation>
    </message>
    <message>
        <source>Left</source>
        <translation>Esquerre</translation>
    </message>
    <message>
        <source>Right</source>
        <translation>Dret</translation>
    </message>
    <message>
        <source>Tap zones</source>
        <translation>Zones de pulsació</translation>
    </message>
    <message>
        <source>Short tap</source>
        <translation>Pulsació curta</translation>
    </message>
    <message>
        <source>Long tap</source>
        <translation>Pulsació llarga</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Cancel·lar</translation>
    </message>
    <message>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <source>Off</source>
        <translation>Deshabilitat</translation>
    </message>
    <message>
        <source>Algorythmic</source>
        <translation>Algorítmica</translation>
    </message>
    <message>
        <source>The quick brown fox jumps over the lazy dog. </source>
        <translation> Jove xef, porti whisky amb quinze glaçons d&apos;hidrogen, coi!</translation>
    </message>
    <message>
        <source>Control</source>
        <translation>Controlar</translation>
    </message>
    <message>
        <source>Swipes</source>
        <translation>Gestos</translation>
    </message>
    <message>
        <source>Right to left</source>
        <translation>De dreta a esquerra</translation>
    </message>
    <message>
        <source>Left to right</source>
        <translation>D’esquerra a dreta</translation>
    </message>
    <message>
        <source>Top to bottom</source>
        <translation>De dalt a baix</translation>
    </message>
    <message>
        <source>Bottom to top</source>
        <translation>De baix a dalt</translation>
    </message>
    <message>
        <source>Keys</source>
        <translation>Tecles</translation>
    </message>
</context>
<context>
    <name>TocDlg</name>
    <message>
        <source>Table of contents</source>
        <translation>Taula de continguts</translation>
    </message>
</context>
</TS>
