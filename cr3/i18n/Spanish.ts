<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es_ES" sourcelanguage="en_US">
<context>
    <name>BookmarkListDialog</name>
    <message>
        <source>Bookmarks</source>
        <translation>Marcadores</translation>
    </message>
    <message>
        <source>Position</source>
        <translation>Posición</translation>
    </message>
    <message>
        <source>Text</source>
        <translation>Texto</translation>
    </message>
</context>
<context>
    <name>CR3View</name>
    <message>
        <source>Error while opening document </source>
        <translation>Error abriendo el documento </translation>
    </message>
    <message>
        <source>Loading: please wait...</source>
        <translation>Abriendo: espere...</translation>
    </message>
</context>
<context>
    <name>FilePropsDialog</name>
    <message>
        <source>Document properties</source>
        <translation>Propiedades del documento</translation>
    </message>
    <message>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <source>Archive name</source>
        <translation>Nombre del comprimido</translation>
    </message>
    <message>
        <source>Archive path</source>
        <translation>Ruta del comprimido</translation>
    </message>
    <message>
        <source>Archive size</source>
        <translation>Tamaño del comprimido (bytes)</translation>
    </message>
    <message>
        <source>File name</source>
        <translation>Nombre</translation>
    </message>
    <message>
        <source>File path</source>
        <translation>Ruta</translation>
    </message>
    <message>
        <source>File size</source>
        <translation>Tamaño (bytes)</translation>
    </message>
    <message>
        <source>File format</source>
        <translation>Formato</translation>
    </message>
    <message>
        <source>File info</source>
        <translation>Información del archivo</translation>
    </message>
    <message>
        <source>Title</source>
        <translation>Título</translation>
    </message>
    <message>
        <source>Author(s)</source>
        <translation>Autor(es)</translation>
    </message>
    <message>
        <source>Series name</source>
        <translation>Serie</translation>
    </message>
    <message>
        <source>Series number</source>
        <translation>Libro</translation>
    </message>
    <message>
        <source>Date</source>
        <translation>Fecha</translation>
    </message>
    <message>
        <source>Genres</source>
        <translation>Géneros</translation>
    </message>
    <message>
        <source>Translator</source>
        <translation>Traductor(es)</translation>
    </message>
    <message>
        <source>Book info</source>
        <translation>Información del libro</translation>
    </message>
    <message>
        <source>Document author</source>
        <translation>Autor del documento</translation>
    </message>
    <message>
        <source>Document date</source>
        <translation>Fecha  del documento</translation>
    </message>
    <message>
        <source>Document source URL</source>
        <translation>Fuente del documento (URL)</translation>
    </message>
    <message>
        <source>OCR by</source>
        <translation>OCR por</translation>
    </message>
    <message>
        <source>Document version</source>
        <translation>Versión del documento</translation>
    </message>
    <message>
        <source>Document info</source>
        <translation>Información del documento</translation>
    </message>
    <message>
        <source>Publication name</source>
        <translation>Nombre de la publicación</translation>
    </message>
    <message>
        <source>Publisher</source>
        <translation>Editor</translation>
    </message>
    <message>
        <source>Publisher city</source>
        <translation>Ciudad del editor</translation>
    </message>
    <message>
        <source>Publication year</source>
        <translation>Año de edición</translation>
    </message>
    <message>
        <source>ISBN</source>
        <translation>ISBN</translation>
    </message>
    <message>
        <source>Publication info</source>
        <translation>Información de la publicación</translation>
    </message>
    <message>
        <source>Custom info</source>
        <translation>Información personalizada</translation>
    </message>
</context>
<context>
    <name>GoToDialog</name>
    <message>
        <source>Position</source>
        <translation>Posición</translation>
    </message>
    <message>
        <source>Page</source>
        <translation>Página</translation>
    </message>
    <message>
        <source>Go</source>
        <translation>Ir</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>Navigation</source>
        <translation>Navegación</translation>
    </message>
    <message>
        <source>Not found</source>
        <translation>Sin resultado</translation>
    </message>
    <message>
        <source>Search pattern is not found in document</source>
        <translation>Patrón no encontrado</translation>
    </message>
    <message>
        <source>Bookmark created</source>
        <translation>Marcador creado</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Borrar</translation>
    </message>
    <message>
        <source>Do you really want to delete the current document?</source>
        <translation>¿Realmente quieres borrar este documento?</translation>
    </message>
    <message>
        <source>Removed</source>
        <translation>Eliminado</translation>
    </message>
    <message>
        <source>Document was removed</source>
        <translation>Documento eliminado</translation>
    </message>
</context>
<context>
    <name>MainWindowClass</name>
    <message>
        <source>Open file</source>
        <translation>Abrir archivo</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>Cerrar</translation>
    </message>
    <message>
        <source>Next page</source>
        <translation>Avanzar página</translation>
    </message>
    <message>
        <source>Previous page</source>
        <translation>Atrasar página</translation>
    </message>
    <message>
        <source>Back</source>
        <translation>Atrás</translation>
    </message>
    <message>
        <source>Forward</source>
        <translation>Adelante</translation>
    </message>
    <message>
        <source>Table of contents</source>
        <translation>Índice</translation>
    </message>
    <message>
        <source>Recent books</source>
        <translation>Libros recientes</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Configuración</translation>
    </message>
    <message>
        <source>Add bookmark</source>
        <translation>Añadir marcador</translation>
    </message>
    <message>
        <source>Bookmarks</source>
        <translation>Marcadores</translation>
    </message>
    <message>
        <source>File properties</source>
        <translation>Datos del archivo</translation>
    </message>
    <message>
        <source>Find text</source>
        <translation>Buscar texto</translation>
    </message>
    <message>
        <source>Show menu</source>
        <translation>Mostrar menú</translation>
    </message>
    <message>
        <source>Empty</source>
        <translation>Vacío</translation>
    </message>
    <message>
        <source>Front light</source>
        <translation>Iluminación</translation>
    </message>
    <message>
        <source>Screen rotation</source>
        <translation>Orientación</translation>
    </message>
    <message>
        <source>Toggle frontlight</source>
        <translation>Iluminación sí/no</translation>
    </message>
    <message>
        <source>Frontlight &gt;</source>
        <translation>Iluminación +</translation>
    </message>
    <message>
        <source>Frontlight &lt;</source>
        <translation>Iluminación −</translation>
    </message>
    <message>
        <source>First page</source>
        <translation>Primera página</translation>
    </message>
    <message>
        <source>Last page</source>
        <translation>Última página</translation>
    </message>
    <message>
        <source>Next chapter</source>
        <translation>Capítulo siguiente</translation>
    </message>
    <message>
        <source>Previous chapter</source>
        <translation>Capítulo anterior</translation>
    </message>
    <message>
        <source>Next 10 pages</source>
        <translation>Avanzar 10 páginas</translation>
    </message>
    <message>
        <source>Previous 10 pages</source>
        <translation>Atrasar 10 páginas</translation>
    </message>
    <message>
        <source>Zoom In</source>
        <translation>Ampliar</translation>
    </message>
    <message>
        <source>Zoom Out</source>
        <translation>Reducir</translation>
    </message>
    <message>
        <source>Position</source>
        <translation>Posición</translation>
    </message>
    <message>
        <source>Toggle inversion</source>
        <translation>Modo nocturno sí/no</translation>
    </message>
    <message>
        <source>Toggle header</source>
        <translation>Encabezado sí/no</translation>
    </message>
    <message>
        <source>Suspend</source>
        <translation>Suspender</translation>
    </message>
    <message>
        <source>Screen rotation 0</source>
        <translation>Girar pantalla 0°</translation>
    </message>
    <message>
        <source>Screen rotation 90</source>
        <translation>Girar pantalla 90°</translation>
    </message>
    <message>
        <source>Screen rotation 180</source>
        <translation>Girar pantalla 180°</translation>
    </message>
    <message>
        <source>Screen rotation 270</source>
        <translation>Girar pantalla 270°</translation>
    </message>
    <message>
        <source>Screen rotation +90</source>
        <translation>Girar pantalla +90°</translation>
    </message>
    <message>
        <source>Screen rotation -90</source>
        <translation>Girar pantalla −90°</translation>
    </message>
    <message>
        <source>Screen rotation +180</source>
        <translation>Girar pantalla +180°</translation>
    </message>
    <message>
        <source>Delete current document</source>
        <translation>Borrar documento actual</translation>
    </message>
    <message>
        <source>Dictionary</source>
        <translation>Diccionario</translation>
    </message>
</context>
<context>
    <name>NavigationBar</name>
    <message>
        <source>%1-%2 (total %3)</source>
        <translation>%1-%2 (total %3)</translation>
    </message>
</context>
<context>
    <name>OpenFileDlg</name>
    <message>
        <source>Open file</source>
        <translation>Abrir archivo</translation>
    </message>
</context>
<context>
    <name>RecentBooksDlg</name>
    <message>
        <source>Recent books</source>
        <translation>Libros recientes</translation>
    </message>
    <message>
        <source>Removed</source>
        <translation>Eliminado</translation>
    </message>
    <message>
        <source>Document was removed</source>
        <translation>Documento eliminado</translation>
    </message>
</context>
<context>
    <name>RecentRemoveDlg</name>
    <message>
        <source>Delete</source>
        <translation>Borrar</translation>
    </message>
    <message>
        <source>Delete record only</source>
        <translation>Borrar sólo registro</translation>
    </message>
    <message>
        <source>Delete record and file</source>
        <translation>Borrar registro y archivo</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
</context>
<context>
    <name>SearchDialog</name>
    <message>
        <source>Search</source>
        <translation>Buscar</translation>
    </message>
    <message>
        <source>Text</source>
        <translation>Texto</translation>
    </message>
    <message>
        <source>Search forward</source>
        <translation>Buscar hacia adelante</translation>
    </message>
    <message>
        <source>Search backward</source>
        <translation>Buscar hacia atrás</translation>
    </message>
    <message>
        <source>Case sensitive</source>
        <translation>Mayúsculas y minúsculas</translation>
    </message>
</context>
<context>
    <name>SettingsDlg</name>
    <message>
        <source>Settings</source>
        <translation>Configuración</translation>
    </message>
    <message>
        <source>Window</source>
        <translation>Ventana</translation>
    </message>
    <message>
        <source>Startup</source>
        <translation>Inicio</translation>
    </message>
    <message>
        <source>Recent book</source>
        <translation>Último libro</translation>
    </message>
    <message>
        <source>Recent books list</source>
        <translation>Lista de libros recientes</translation>
    </message>
    <message>
        <source>Open file</source>
        <translation>Abrir archivo</translation>
    </message>
    <message>
        <source>Do nothing</source>
        <translation>No hacer nada</translation>
    </message>
    <message>
        <source>Language</source>
        <translation type="vanished">Idioma</translation>
    </message>
    <message>
        <source>Formatting</source>
        <translation>Formato</translation>
    </message>
    <message>
        <source>Disable txt auto formatting</source>
        <translation>Desactivar autoformato de texto</translation>
    </message>
    <message>
        <source>Internal styles</source>
        <translation>Estilos internos</translation>
    </message>
    <message>
        <source>Embedded fonts</source>
        <translation>Fuentes incrustadas</translation>
    </message>
    <message>
        <source>Page</source>
        <translation>Página</translation>
    </message>
    <message>
        <source>Header</source>
        <translation>Encabezado</translation>
    </message>
    <message>
        <source>Show</source>
        <translation>Mostrar</translation>
    </message>
    <message>
        <source>Font</source>
        <translation>Fuente</translation>
    </message>
    <message>
        <source>Face</source>
        <translation>Tipo</translation>
    </message>
    <message>
        <source>Size</source>
        <translation>Tamaño</translation>
    </message>
    <message>
        <source>Elements</source>
        <translation>Elementos</translation>
    </message>
    <message>
        <source>Chapter marks</source>
        <translation>Marcador de capítulos</translation>
    </message>
    <message>
        <source>Book name</source>
        <translation>Nombre del libro</translation>
    </message>
    <message>
        <source>Clock</source>
        <translation>Reloj</translation>
    </message>
    <message>
        <source>Position percent</source>
        <translation>Posición en porcentaje</translation>
    </message>
    <message>
        <source>Page number/count</source>
        <translation>Número de página/total</translation>
    </message>
    <message>
        <source>Battery status</source>
        <translation>Carga batería</translation>
    </message>
    <message>
        <source>In percent</source>
        <translation>En porcentaje</translation>
    </message>
    <message>
        <source>Footer</source>
        <translation>Pie</translation>
    </message>
    <message>
        <source>Misc</source>
        <translation>Miscelánea</translation>
    </message>
    <message>
        <source>View Mode</source>
        <translation>Modo de vista</translation>
    </message>
    <message>
        <source>One page</source>
        <translation>Una página</translation>
    </message>
    <message>
        <source>Two pages</source>
        <translation>Dos páginas</translation>
    </message>
    <message>
        <source>Update interval</source>
        <translation>Referesco (páginas)</translation>
    </message>
    <message>
        <source>Negative</source>
        <translation>Modo nocturno</translation>
    </message>
    <message>
        <source>Styles</source>
        <translation>Estilos</translation>
    </message>
    <message>
        <source>Gamma</source>
        <translation>Gamma</translation>
    </message>
    <message>
        <source>Aa</source>
        <translation>Aa</translation>
    </message>
    <message>
        <source>Disabled</source>
        <translation>Deshabilitado</translation>
    </message>
    <message>
        <source>On for large fonts</source>
        <translation>En fuentes grandes</translation>
    </message>
    <message>
        <source>On for all fonts</source>
        <translation>En todas las fuentes</translation>
    </message>
    <message>
        <source>Hinting</source>
        <translation>Optimizar</translation>
    </message>
    <message>
        <source>Bytecode</source>
        <translation>Usar código incrustado</translation>
    </message>
    <message>
        <source>Auto</source>
        <translation>Auto</translation>
    </message>
    <message>
        <source>Bold</source>
        <translation>Negrita</translation>
    </message>
    <message>
        <source>Kerning</source>
        <translation>Interletraje</translation>
    </message>
    <message>
        <source>Layout</source>
        <translation>Formato</translation>
    </message>
    <message>
        <source>Hyphenation</source>
        <translation>Divisiones</translation>
    </message>
    <message>
        <source>Spacing</source>
        <translation>Interlínea</translation>
    </message>
    <message>
        <source>Space</source>
        <translation>Espacios</translation>
    </message>
    <message>
        <source>Floating punctuation</source>
        <translation>Ajustar puntuación flotante</translation>
    </message>
    <message>
        <source>Margins</source>
        <translation>Márgenes</translation>
    </message>
    <message>
        <source>Top</source>
        <translation>Arriba</translation>
    </message>
    <message>
        <source>Bottom</source>
        <translation>Abajo</translation>
    </message>
    <message>
        <source>Left</source>
        <translation>Izquierda</translation>
    </message>
    <message>
        <source>Right</source>
        <translation>Derecha</translation>
    </message>
    <message>
        <source>Tap zones</source>
        <translation>Zonas de toque</translation>
    </message>
    <message>
        <source>Short tap</source>
        <translation>Toque corto</translation>
    </message>
    <message>
        <source>Long tap</source>
        <translation>Toque largo</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <source>Off</source>
        <translation>Deshabilitado</translation>
    </message>
    <message>
        <source>Algorythmic</source>
        <translation>Algorítmica</translation>
    </message>
    <message>
        <source>The quick brown fox jumps over the lazy dog. </source>
        <translation>El veloz murciélago hindú comía feliz cardillo y kiwi. La cigüeña tocaba el saxofón detrás del palenque de paja.</translation>
    </message>
    <message>
        <source>Control</source>
        <translation>Controles</translation>
    </message>
    <message>
        <source>Swipes</source>
        <translation>Gestos</translation>
    </message>
    <message>
        <source>Right to left</source>
        <translation>De derecha a izquierda</translation>
    </message>
    <message>
        <source>Left to right</source>
        <translation>De izquierda a derecha</translation>
    </message>
    <message>
        <source>Top to bottom</source>
        <translation>De arriba a abajo</translation>
    </message>
    <message>
        <source>Bottom to top</source>
        <translation>De abajo a arriba</translation>
    </message>
    <message>
        <source>Keys</source>
        <translation>Teclas</translation>
    </message>
</context>
<context>
    <name>TocDlg</name>
    <message>
        <source>Table of contents</source>
        <translation>Índice</translation>
    </message>
</context>
</TS>
