<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>BookmarkListDialog</name>
    <message>
        <location filename="../bookmarklistdlg.ui" line="20"/>
        <location filename="../bookmarklistdlg.ui" line="26"/>
        <source>Bookmarks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../bookmarklistdlg.ui" line="81"/>
        <source>Position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../bookmarklistdlg.ui" line="86"/>
        <source>Text</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CR3View</name>
    <message>
        <location filename="../cr3widget.cpp" line="433"/>
        <source>Error while opening document </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cr3widget.cpp" line="1037"/>
        <source>Loading: please wait...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FilePropsDialog</name>
    <message>
        <location filename="../filepropsdlg.ui" line="20"/>
        <source>Document properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filepropsdlg.ui" line="64"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="131"/>
        <source>Archive name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="132"/>
        <source>Archive path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="133"/>
        <source>Archive size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="134"/>
        <source>File name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="135"/>
        <source>File path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="136"/>
        <source>File size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="137"/>
        <source>File format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="139"/>
        <source>File info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="141"/>
        <source>Title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="142"/>
        <source>Author(s)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="143"/>
        <source>Series name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="144"/>
        <source>Series number</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="145"/>
        <source>Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="146"/>
        <source>Genres</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="147"/>
        <source>Translator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="148"/>
        <source>Book info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="150"/>
        <source>Document author</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="151"/>
        <source>Document date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="152"/>
        <source>Document source URL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="153"/>
        <source>OCR by</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="154"/>
        <source>Document version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="155"/>
        <source>Document info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="157"/>
        <source>Publication name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="158"/>
        <source>Publisher</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="159"/>
        <source>Publisher city</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="160"/>
        <source>Publication year</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="161"/>
        <source>ISBN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="162"/>
        <source>Publication info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filepropsdlg.cpp" line="164"/>
        <source>Custom info</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GoToDialog</name>
    <message>
        <location filename="../gotodialog.ui" line="14"/>
        <location filename="../gotodialog.ui" line="20"/>
        <source>Position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gotodialog.ui" line="52"/>
        <source>Page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gotodialog.ui" line="96"/>
        <source>Go</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gotodialog.ui" line="103"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.cpp" line="216"/>
        <source>Navigation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="271"/>
        <source>Bookmark created</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="306"/>
        <source>Not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="306"/>
        <source>Search pattern is not found in document</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="770"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="770"/>
        <source>Do you really want to delete the current document?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="776"/>
        <source>Removed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="776"/>
        <source>Document was removed</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindowClass</name>
    <message>
        <location filename="../mainwindow.ui" line="44"/>
        <source>Open file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="49"/>
        <location filename="../mainwindow.ui" line="52"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="60"/>
        <source>Next page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="65"/>
        <source>Previous page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="70"/>
        <source>Back</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="75"/>
        <source>Forward</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="80"/>
        <source>Table of contents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="88"/>
        <source>Recent books</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="93"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="98"/>
        <location filename="../mainwindow.ui" line="101"/>
        <source>Add bookmark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="106"/>
        <source>Bookmarks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="111"/>
        <source>File properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="116"/>
        <location filename="../mainwindow.ui" line="119"/>
        <source>Find text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="124"/>
        <source>Show menu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="132"/>
        <source>Empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="143"/>
        <location filename="../mainwindow.ui" line="146"/>
        <source>Front light</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="151"/>
        <source>Screen rotation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="156"/>
        <source>Toggle frontlight</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="161"/>
        <source>Frontlight &gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="166"/>
        <source>Frontlight &lt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="171"/>
        <source>First page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="176"/>
        <source>Last page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="181"/>
        <source>Next chapter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="186"/>
        <source>Previous chapter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="191"/>
        <source>Next 10 pages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="196"/>
        <source>Previous 10 pages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="201"/>
        <source>Zoom In</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="206"/>
        <source>Zoom Out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="211"/>
        <location filename="../mainwindow.ui" line="214"/>
        <source>Position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="219"/>
        <source>Toggle inversion</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="224"/>
        <source>Toggle header</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="229"/>
        <source>Suspend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="234"/>
        <source>Screen rotation 0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="239"/>
        <source>Screen rotation 90</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="244"/>
        <source>Screen rotation 180</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="249"/>
        <source>Screen rotation 270</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="254"/>
        <source>Screen rotation +90</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="259"/>
        <source>Screen rotation -90</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="264"/>
        <source>Screen rotation +180</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="269"/>
        <source>Delete current document</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="274"/>
        <source>Dictionary</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NavigationBar</name>
    <message>
        <location filename="../navigationbar.cpp" line="92"/>
        <location filename="../navigationbar.cpp" line="109"/>
        <source>%1-%2 (total %3)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OpenFileDlg</name>
    <message>
        <location filename="../openfiledlg.ui" line="20"/>
        <location filename="../openfiledlg.ui" line="26"/>
        <source>Open file</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RecentBooksDlg</name>
    <message>
        <location filename="../recentdlg.ui" line="26"/>
        <location filename="../recentdlg.ui" line="32"/>
        <source>Recent books</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../recentdlg.cpp" line="236"/>
        <source>Removed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../recentdlg.cpp" line="236"/>
        <source>Document was removed</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RecentRemoveDlg</name>
    <message>
        <location filename="../recentremovedlg.ui" line="14"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../recentremovedlg.ui" line="20"/>
        <source>Delete record only</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../recentremovedlg.ui" line="27"/>
        <source>Delete record and file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../recentremovedlg.ui" line="47"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SearchDialog</name>
    <message>
        <location filename="../searchdlg.ui" line="20"/>
        <location filename="../searchdlg.ui" line="26"/>
        <source>Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../searchdlg.ui" line="51"/>
        <source>Text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../searchdlg.ui" line="61"/>
        <source>Case sensitive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../searchdlg.ui" line="68"/>
        <source>Search forward</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../searchdlg.ui" line="81"/>
        <source>Search backward</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsDlg</name>
    <message>
        <location filename="../settings.ui" line="26"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="42"/>
        <source>Window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="69"/>
        <source>Startup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="83"/>
        <source>Recent book</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="88"/>
        <source>Recent books list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="93"/>
        <source>Open file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="98"/>
        <source>Do nothing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="108"/>
        <source>Formatting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="114"/>
        <source>Disable txt auto formatting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="121"/>
        <source>Internal styles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="128"/>
        <source>Embedded fonts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="152"/>
        <source>Page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="184"/>
        <source>Header</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="190"/>
        <location filename="../settings.ui" line="362"/>
        <source>Show</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="197"/>
        <location filename="../settings.ui" line="574"/>
        <source>Font</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="212"/>
        <location filename="../settings.ui" line="588"/>
        <source>Face</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="229"/>
        <location filename="../settings.ui" line="611"/>
        <source>Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="261"/>
        <source>Elements</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="267"/>
        <source>Chapter marks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="274"/>
        <source>Book name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="281"/>
        <source>Clock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="288"/>
        <source>Position percent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="295"/>
        <source>Page number/count</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="310"/>
        <source>Battery status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="317"/>
        <source>In percent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="356"/>
        <source>Footer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="383"/>
        <source>Misc</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="400"/>
        <source>View Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="414"/>
        <source>One page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="419"/>
        <source>Two pages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="427"/>
        <source>Update interval</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="518"/>
        <source>Negative</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="545"/>
        <source>Styles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="647"/>
        <source>Gamma</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="692"/>
        <source>Aa</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="706"/>
        <location filename="../settings.ui" line="738"/>
        <source>Disabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="711"/>
        <source>On for large fonts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="716"/>
        <source>On for all fonts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="724"/>
        <source>Hinting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="743"/>
        <source>Bytecode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="748"/>
        <source>Auto</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="766"/>
        <source>Bold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="779"/>
        <source>Kerning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="789"/>
        <source>Layout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="797"/>
        <source>Hyphenation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="817"/>
        <source>Spacing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="856"/>
        <location filename="../settings.ui" line="1366"/>
        <source>Space</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="897"/>
        <source>Floating punctuation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="918"/>
        <source>Margins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="924"/>
        <source>Top</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="950"/>
        <source>Bottom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="976"/>
        <source>Left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1002"/>
        <source>Right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1049"/>
        <source>Control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1078"/>
        <source>Tap zones</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1091"/>
        <source>Short tap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1096"/>
        <source>Long tap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1125"/>
        <source>Swipes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1132"/>
        <source>Left to right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1137"/>
        <source>Right to left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1142"/>
        <source>Top to bottom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1147"/>
        <source>Bottom to top</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1172"/>
        <source>Keys</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1415"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1422"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="170"/>
        <source>Off</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="172"/>
        <source>Algorythmic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="180"/>
        <source>The quick brown fox jumps over the lazy dog. </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TocDlg</name>
    <message>
        <location filename="../tocdlg.ui" line="20"/>
        <location filename="../tocdlg.ui" line="26"/>
        <source>Table of contents</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
