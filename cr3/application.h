#include <QTimer>

#include "Application.h"

class CrApplication : public Application
{
    Q_OBJECT

    const char* argv0;
    QTimer idleTimer;

public:
    CrApplication(int& argc, char** argv);
    ~CrApplication();

protected:
    void customInit();
    bool notify(QObject * receiver, QEvent * event);

private slots:
    void onIdle();
};
