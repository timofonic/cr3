#ifndef PROPS_H
#define PROPS_H

#define PROP_APP_START_ACTION           "cr3.app.start.action"
#define PROP_APP_UI_ORIENTATION         "cr3.app.ui.orientation"
#define PROP_APP_UI_SWIPES_X_WEIGHT     "cr3.app.ui.swipes.xweight"
#define PROP_APP_UI_SWIPES_Y_WEIGHT     "cr3.app.ui.swipes.yweight"
#define PROP_APP_UI_KEY_MAPPING         "cr3.app.ui.key.%1.%2"
#define PROP_APP_UI_ADISP_TIME          "cr3.app.ui.adisp.time"
#define PROP_APP_UI_TOC_MODE            "cr3.app.ui.toc.mode"

#endif // PROPS_H
